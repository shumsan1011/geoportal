import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { WidgetsModule } from './modules/widgets/widgets.module';
import { APIService } from './services/api.service';
import { MapService } from './services/map.service';
import { MapComponent } from './components/map/map.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { StaticPageLayoutComponent } from './components/static-page-layout/static-page-layout.component';
import { DocFormComponent } from 'app/shared/components/doc-form/doc-form.component';
import { DocFormTableEditingWrapperComponent } from 'app/shared/components/doc-form-table-editing-wrapper/doc-form-table-editing-wrapper.component';
import { TablelistComponent } from 'app/shared/components/tablelist/tablelist.component';
import { ModalConfirmationComponent } from 'app/shared/components/modal-confirmation/modal-confirmation.component';
import { TableDlgComponent } from 'app/shared/components/table-dlg/table-dlg.component';
import { TableModelComponent } from 'app/shared/components/table-model/table-model.component';

@NgModule({
    imports: [RouterModule, CommonModule, FormsModule, NgbModule, WidgetsModule],
    declarations: [
        MainMenuComponent,
        StaticPageLayoutComponent,
        DocFormComponent,
        DocFormTableEditingWrapperComponent,
        TablelistComponent,
        ModalConfirmationComponent,
        TableDlgComponent,
        TableModelComponent
    ],
    exports: [
        MainMenuComponent,
        DocFormComponent,
        StaticPageLayoutComponent,
        DocFormTableEditingWrapperComponent,
        TablelistComponent,
        ModalConfirmationComponent,
        TableDlgComponent,
        TableModelComponent
    ],
    entryComponents: [TableDlgComponent, DocFormTableEditingWrapperComponent],
    providers: [APIService, MapService]
})
export class SharedModule {}
