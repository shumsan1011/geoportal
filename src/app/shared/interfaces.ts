export interface AuthenticationReply {
    accessToken: string;
    refreshToken: string;
}

export interface Metadata {
    tablename?: string;
    dataset_id?: number;
    schema?: string;
    title?: string;
    print_template?: Array<string>;
    columns?: Array<MetadataColumns>;
}

export interface DataDocument {
    id: number;
}

// string for f-edit and NumberFilter for f-number
export type WidgetFilter = string | NumberFilter;

export interface MetadataColumns {
    fieldname: string;
    title?: string;
    description?: string;
    visible?: boolean;
    type?: string;
    required?: boolean;
    widget?: {
        name: string;
        properties?: {
            dataset_id?: number;
            control_type?: string;
            db_field?: Array<string>;
            reftablename?: string;
            autocomplete?: boolean;
            autocompletefield?: string;
        };
    };
    condition?: string;
    conditions?: Array<string>;
    size?: number;
    tablename: string;
    sqltablename: string;
    sqlname: string;
    parentfield?: string;
    filter?: Array<string>;
    sortingOrder?: SortingField;
    filterValueList?: Array<WidgetFilter>;
}

export interface MetadataFilter {
    fieldname: string;
    valueList: Array<WidgetFilter>;
}

export interface SortingField {
    className?: string; // like 'fa-sort-asc'
    direction: 'ASC' | 'DESC' | '';
}

export interface MetadataSorting {
    fieldname: string;
    sorting: SortingField;
}

export enum ModalResult {
    mrNone = 0,
    mrOk,
    mrCancel,
    mrAbort,
    mrRetry,
    mrIgnore,
    mrYes,
    mrNo
}

export interface DatasetData {
    aaData: Array<any>;
}

export enum SignOptions {
    Equals = '==',
    NotEqualTo = '!=',
    GreaterThan = '>',
    LessThan = '<',
    GreaterThanOrEqualTo = '>=',
    LessThanOrEqualTo = '<='
}

export enum LogicElements {
    Empty = 'EMPTY',
    And = 'AND',
    Or = 'OR',
    Not = 'NOT'
}

export interface NumberFilter {
    signOptionLeft: SignOptions;
    signOptionRight: SignOptions;
    valueLeft: string;
    valueRight: string;
}
