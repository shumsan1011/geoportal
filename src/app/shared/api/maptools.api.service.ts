import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { State, Store } from '@ngrx/store';
import {NgForage, NgForageCache, NgForageConfig, CachedItem} from 'ngforage';
import 'rxjs/add/operator/map';

import { AppConfig } from './../../app.config';
import { AppActions } from '../../store/app.actions';
import { AppState } from '../../store/app.state';

@Injectable()
export class MapToolsAPIService {
  constructor(private http: HttpClient, private appActions: AppActions) {}

  /**
   * Creates MAP file for table geometry field.
   */
  createMAPFileForField(metaId: number, unique: string, fieldName: string, mapStyle: string = '') {
    return this.http.post(`${AppConfig.APIPath}/geothemes/create_map_for_field`, {
      metaid: metaId,
      unique,
      fieldname: fieldName,
      mapstyle: mapStyle
    }).toPromise();
  }
}
