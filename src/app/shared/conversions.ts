export const addzero = (v, cnt) => {
	v=v+'';
	while (cnt>v.length){
	  v='0'+v;
	}  
	return v;
}

export const addsec = (v) => {  
	v=v+'';
	let ps=v.split('.');
	for(let i=ps[0].length; i<2; i++){
		v='0'+v;
	}

	while (5>v.length){
		if(v.length==2)
			v=v+'.';		
		else
			v=v+'0';
	}  
	return v;
}

export const Dec2minute_sec = (val) => {
	if(val<0)
		val=-val;
	var lon_gr = addzero(Math.floor(val),3);
	var lon_mn = addzero(Math.floor((val-lon_gr)*60),2);	
	var lon_sec = addsec(Math.round((((val-lon_gr)*60-lon_mn)*60)*100)/100);
	
	var value =lon_gr+"°"+lon_mn+"'"+lon_sec+"''";
	return value;
};

export const minute_sec2Dec = (val) => {
	if(val ==undefined || val=='')
		return 0;
	var sign=val.toUpperCase().charAt(0);
	sign=sign=='E' || sign=='N';
	val=val.substr(1);
	let arr = val.split(/[\D]/);
	var sec = parseFloat(arr[2]+'.'+arr[3]);
	sec = sec/3600;

	var mn = arr[1]/60;
	var g = parseInt(arr[0],10)+mn+sec;	
	if(!sign)
		g=-g;	
	return g;
}

export const wkt2decimal = (val) => {
	var re=/(MULTI){0,}POINT[\s]{0,1}\((-?[0-9]*\.?[0-9]*)\s(-?[0-9]*\.?[0-9]*)\)/
	var arr = re.exec(val);
	let result = {lat: '0', lon: '0'};
	if (arr) {
		if (arr.length === 3) {
			result = {lon:arr[1], lat:arr[2]};
		} else if (arr.length === 4) {
			result = {lon:arr[2], lat:arr[3]};
		} else {
			throw 'Invalid case';
		}
	}

	return result;
};

export const wkt2minute_sec = (val) => {
	var re = /MULTIPOINT\((-?[0-9]*\.?[0-9]*)\s(-?[0-9]*\.?[0-9]*)\)/
	var arr = re.exec(val);			
	if (arr==null || arr.length<3) return {lat:0, lon:0};

	let lat=Dec2minute_sec(arr[2]);
	let lon=Dec2minute_sec(arr[1]);
	
	if(parseFloat(arr[2]) < 0)
		lat='S'+lat;
	else
    lat='N'+lat;

	if(parseFloat(arr[1]) < 0)
		lon='W'+lon;
	else
    lon='E'+lon;

	return {lat:lat, lon:lon};
}

export const ToDec = (string) => {
	let in_arr = string.split(' ');
	
	var lon_vl = in_arr[1];	
	var lon_sign=lon_vl.toUpperCase().charAt(0);
	if(lon_sign=='E' || lon_sign=='W'){		
		lon_vl=lon_vl.substr(1);
	}
	else
		lon_sign='E';
	
	var lat_vl = in_arr[0];
	var lat_sign=lat_vl.toUpperCase().charAt(0);
	if(lat_sign=='N' || lat_sign=='S'){		
		lat_vl=lat_vl.substr(1);
	}
	else
		lat_sign='N';
		
	let arr = lon_vl.split(/[\D]/);
	var sec=arr[2]+'.'+arr[3];
	var lon_sec = parseFloat(sec)/3600;
	var lon_mn = arr[1]/60;
	var lon = parseInt(arr[0],10)+lon_mn+lon_sec;	
	if(lon_sign!='E')
		lon=-lon;
	
	arr = lat_vl.split(/[\D]/);
	var sec=arr[2]+'.'+arr[3];
	var lan_sec = parseFloat(sec)/3600;
	var lan_mn = arr[1]/60;
	var lat = parseInt(arr[0],10)+lan_mn+lan_sec;
	if(lat_sign!='N')
		lat=-lat;
					
	var value = lon + " " + lat;
	return value;
};

export const FromDec = (string) => {
  let arr;
	if(string=='')
		arr={0:'0',1:'0'};	
	else
		arr  =  string.split(' ');
	
	let lan_sign='N';
	if(arr[1]+0<0)
		lan_sign='S';

	let lon_sign='E';
	if(arr[0]+0<0)
		lon_sign='W';
		
	var lon_gr = addzero(Math.floor(arr[0]),3);
	var lon_mn = addzero(Math.floor((arr[0]-lon_gr)*60),2);	
	var lon_sec = addsec(Math.round((((arr[0]-lon_gr)*60-lon_mn)*60)*100)/100);
		 
	var lan_gr = addzero(Math.floor(arr[1]),3);
	var lan_mn = addzero(Math.floor((arr[1]-lan_gr)*60),2);
	var lan_sec = addsec(Math.round((((arr[1]-lan_gr)*60-lan_mn)*60)*100)/100);
	
	var value = lan_sign + lan_gr + "°" + lan_mn + "'" + lan_sec + "''" + " " + lon_sign + lon_gr + "°" + lon_mn + "'" + lon_sec + "''";
	return value;
};

/**
* tranlsit - trnforming cyrillic to latin string
*/
export const translit = (str) => {
	if (str === undefined) {
		str = 'notdef';
	};
	if (str == undefined) {
		str = 'notdef';
	};
	let transl = new Array();
    transl['А']='A';     transl['а']='a';
    transl['Б']='B';     transl['б']='b';
    transl['В']='V';     transl['в']='v';
    transl['Г']='G';     transl['г']='g';
    transl['Д']='D';     transl['д']='d';
    transl['Е']='E';     transl['е']='e';
    transl['Ё']='Yo';    transl['ё']='yo';
    transl['Ж']='Zh';    transl['ж']='zh';
    transl['З']='Z';     transl['з']='z';
    transl['И']='I';     transl['и']='i';
    transl['Й']='J';     transl['й']='j';
    transl['К']='K';     transl['к']='k';
    transl['Л']='L';     transl['л']='l';
    transl['М']='M';     transl['м']='m';
    transl['Н']='N';     transl['н']='n';
    transl['О']='O';     transl['о']='o';
    transl['П']='P';     transl['п']='p';
    transl['Р']='R';     transl['р']='r';
    transl['С']='S';     transl['с']='s';
    transl['Т']='T';     transl['т']='t';
    transl['У']='U';     transl['у']='u';
    transl['Ф']='F';     transl['ф']='f';
    transl['Х']='X';     transl['х']='x';
    transl['Ц']='C';     transl['ц']='c';
    transl['Ч']='Ch';    transl['ч']='ch';
    transl['Ш']='Sh';    transl['ш']='sh';
    transl['Щ']='Shh';    transl['щ']='shh';
    transl['Ъ']='';     transl['ъ']='';
    transl['Ы']='Y';    transl['ы']='y';
    transl['Ь']='';    transl['ь']='';
    transl['Э']='E';    transl['э']='e';
    transl['Ю']='Yu';    transl['ю']='yu';
    transl['Я']='Ya';    transl['я']='ya';
	transl['_']='_';  transl[' ']='_';
    var result='';
    for(let i = 0; i < str.length; i++) {
        if (transl[str[i]] != undefined) {
			result+=transl[str[i]]; 
		} else {
			result+=str[i];
		}
    }
    return result;
}


/**
*	It is an util function for obtaining correct object name
*
*	@param name - input name to make correct
*	@return correct name
*/
export const getcorrectobjname = (name) => {
	name=name.toLowerCase();
	
	var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
	var digits='0123456789';
	var res=''
	name=translit(name);
	for(var i=0; i<name.length; i++){
		if (res.length==0 && digits.indexOf(name.charAt(i))!=-1 ) continue;
		if(chars.indexOf(name.charAt(i))!=-1)
			res=res+name.charAt(i);
		if (res.length>15) break;
	}
	return res;	
}
