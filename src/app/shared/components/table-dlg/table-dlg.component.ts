import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalResult } from 'app/shared/interfaces';

@Component({
  selector: 'app-table-dlg',
  templateUrl: './table-dlg.component.html'
})
export class TableDlgComponent implements OnInit {
  private currentTableId: Number;
  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {}

  docSubmit() {
    this.activeModal.close({ choosed: ModalResult.mrOk, dataset_id: this.currentTableId });
  }

  changecurentval(tableId: String) {
    this.currentTableId = Number(tableId);
  }
}
