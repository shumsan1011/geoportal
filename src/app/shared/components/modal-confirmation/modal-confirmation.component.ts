import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalResult } from 'app/shared/interfaces';

@Component({
  selector: 'app-modal-confirmation',
  templateUrl: './modal-confirmation.component.html'
})
export class ModalConfirmationComponent implements OnInit {
  @Input() title: String;
  @Input() body: String;

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {
  }

  yesAnswer() {
    this.activeModal.close(ModalResult.mrYes);
  }

  noAnswer() {
    this.activeModal.close(ModalResult.mrNo);
  }

}
