import { Component, Input, OnInit, ViewContainerRef, AfterViewInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { WidgetComponent } from 'app/shared/modules/widgets/components/widget/widget.component';
import { APIService } from 'app/shared/services/api.service';
import { Metadata } from 'app/shared/interfaces';
import { AppActions } from 'app/store/app.actions';

import { State, Store } from '@ngrx/store';
import { AppState } from 'app/store/app.state';
import { stateGetter } from 'app/store/app.reducer';

export class Document {
  constructor(public id: number) { }
}

/*
@todo Use the doc-form component and clean the component up
*/

@Component({
  selector: 'doc-form',
  templateUrl: 'doc-form-table-editing-wrapper.component.html',
  styleUrls: ['doc-form-table-editing-wrapper.component.scss'],
  providers: [APIService]
})
export class DocFormTableEditingWrapperComponent implements OnInit {
  @ViewChild('mapPlacement', { read: ViewContainerRef }) mapPlacementContainer;

  model = new Document(-1);
  history: string[] = [];
  formData: object = {};
  mapRef: any;

  // TODO Should add interface
  @Input() meta: Metadata = {};
  @Input() doc = {};

  @ViewChildren(WidgetComponent) childrenWidgets: QueryList<WidgetComponent>;

  constructor(public activeModal: NgbActiveModal, private store: Store<AppState>, private DocService: APIService, private appActions: AppActions) {}

  docSubmit() {
    this.childrenWidgets.forEach((item) => {
      console.log(item.attr.fieldname, item.getValue());
      this.doc[item.attr.fieldname] = item.getValue();
    });
    console.log('ok');

    if('id' in this.doc && this.doc['id']!=-1){
      this.DocService.update(this.meta.dataset_id, this.doc).then(res => {
        console.log(res);
        this.appActions.updateTableData( this.meta.dataset_id );
      });
    } else {
      this.DocService.add(this.meta.dataset_id, this.doc).then(res => {
        console.log(res);
        this.appActions.updateTableData( this.meta.dataset_id );
      });
    }

    this.docCancel();
  }

  docCancel() {
    this.mapPlacementContainer.detach(this.mapPlacementContainer.indexOf(this.mapRef.hostView));
    this.appActions.setFrontPageMapHost({ host: 'regular' });
    this.activeModal.close('Close click');
  }

  ngOnInit() {
    this.appActions.setFrontPageMapHost({ host: 'embedded' });
    this.store.select(stateGetter('maps', 'frontPageMap', 'ref')).take(1).subscribe((data) => {
      this.mapRef = data;
      this.mapPlacementContainer.insert(data.hostView);
    });
  }
}
