import { Component } from '@angular/core';

@Component({
  selector: 'gp-static-page-layout',
  template: `<div class="layout__menu" main-menu></div>
  <div class="container">
    <div class="row">
      <div class="col">
        <ng-content></ng-content>
      </div>
    </div>
  </div>`
})
export class StaticPageLayoutComponent {}