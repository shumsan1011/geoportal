import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppActions } from 'app/store/app.actions';
import { Metadata, MetadataColumns, ModalResult } from 'app/shared/interfaces';
import { APIService } from 'app/shared/services/api.service';
import { getcorrectobjname } from 'app/shared/conversions';
import { ModalConfirmationComponent } from '../modal-confirmation/modal-confirmation.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-table-model',
  templateUrl: './table-model.component.html',
  styleUrls: ['./table-model.component.scss']
})
export class TableModelComponent implements OnInit {
  dataset_id: number;
  step: number = 1;
  newtable: boolean = true;
  selectedtable: boolean = false;
  tryselectingtable: boolean = false;
  validating: boolean = false;
  meta: Metadata = {};
  cur_column: any;
  complexForm: FormGroup;
  errorMsg:string ='';
  widgets = [
    { id: 'edit', name: 'edit' },
    { id: 'number', name: 'number' },
    { id: 'polygon', name: 'polygon' },
    { id: 'line', name: 'line' },
    { id: 'select', name: 'select' },
    { id: 'classify', name: 'classify' }
  ];

  constructor(
    public activeModal: NgbActiveModal,
    private appActions: AppActions,
    private datatableService: APIService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {}

  modelSubmit() {
    this.validating = true;
    //this.appActions.showTable({ dataset_id: this.dataset_id });
    if (!this.meta.title) {
      this.step = 2;
      return;
    }
    if (!this.meta.columns || this.meta.columns.length == 0) {
      this.step = 3;
      return;
    }
    if (!this.meta.tablename || this.meta.tablename == '')
      this.meta.tablename = getcorrectobjname(this.meta.title);

    this.meta.columns.forEach(element => {
      if (!element.fieldname || element.fieldname == '')
        element.fieldname = getcorrectobjname(element.title);
      if (!element.sqlname || element.sqlname == '')
      element.sqlname=element.fieldname;
    });
    let documentjson = JSON.stringify(this.meta);
    this.datatableService.createdatatable( this.meta).then(res => {
      console.log(res);
      this.activeModal.close('Save click');
      //this.appActions.updateTable( this.meta.dataset_id );
    }).catch((msg) => 
      this.errorMsg = msg.error.error
  );
    //alert(documentjson);
    console.log(this.dataset_id);
    
  }

  changetab(dataset_id: number) {
    this.dataset_id = dataset_id;
    this.selectedtable = true;
    this.datatableService.meta(this.dataset_id).then(serviceMeta => {
      this.meta = serviceMeta;
      this.meta.columns.forEach(element => {
        if (!element.widget) element.widget = {name:''};
        if (!element.widget.properties) element.widget.properties = {};
      });
    });
  }
  next() {
    if(this.step==1 && !this.newtable && !this.selectedtable){
      this.tryselectingtable = true;
      return;
    }
    else if(this.step==1 && this.newtable && this.meta.dataset_id){
      this.meta={};
    }
    this.step++;
  }
  prev() {
    this.step--;
  }

  addcolumn() {
    let column:MetadataColumns = {
      title: 'new column',
      visible: true,
      fieldname:'',
      tablename:'',
      sqltablename:'',
      sqlname:'',
      widget: { name: 'edit', properties: {} }      
    };
    if (!this.meta.columns) this.meta.columns = [];
    this.meta.columns.push(column);
  }

  editcolumn(column) {
    column.edit = true;
    console.log('ok');
    this.cur_column = column;
  }
  savetitlecolumn($event) {
    this.cur_column.edit = false;
    this.cur_column.title = $event.target.value;
  }
  savewidgetcolumn($event, column) {
    column.widget.name = $event.target.value;
  }

  openConfirmationModalForm(
    title: String = 'Geoportal',
    body: String
  ): Promise<any> {
    const modalRef = this.modalService.open(ModalConfirmationComponent);
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.body = body;
    return modalRef.result;
  }
  deletecolumn(column) {
    this.openConfirmationModalForm(
      'Deleting column',
      'Are you sure you want to delete this column?'
    ).then(result => {
      if (result === ModalResult.mrYes) {
        let ind = this.meta.columns.indexOf(column);
        if (ind != -1) this.meta.columns.splice(ind, 1);
      }
    });
  }

  detail(column) {
    if (column.propertyediting) column.propertyediting = false;
    else column.propertyediting = true;
  }
}
