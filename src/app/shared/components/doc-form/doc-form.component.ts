import { Component, Output, Input, OnInit, EventEmitter, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { WidgetComponent } from 'app/shared/modules/widgets/components/widget/widget.component';
import { APIService } from 'app/shared/services/api.service';
import { Metadata } from 'app/shared/interfaces';
import { AppActions } from 'app/store/app.actions';

import { State, Store } from '@ngrx/store';
import { AppState } from 'app/store/app.state';
import { stateGetter } from 'app/store/app.reducer';

@Component({
  selector: 'app-doc-form',
  templateUrl: './doc-form.component.html',
  styleUrls: ['./doc-form.component.scss']
})
export class DocFormComponent implements OnInit {

  public modes = {
    ADD: 1,
    UPDATE: 2
  };

  public mode: number;

  @Input() meta: Metadata = {};
  @Input() doc = {};

  @Output() onSuccessfullSubmition: EventEmitter<any> = new EventEmitter();

  loading = false;
  formDataIsComplete = true;
  serverError: any = false;

  @ViewChildren(WidgetComponent) childrenWidgets: QueryList<WidgetComponent>;

  constructor(private store: Store<AppState>, private apiService: APIService, private appActions: AppActions) {
    if ('id' in this.doc && this.doc['id'] != -1) {
      this.mode = this.modes.UPDATE;
    } else {
      this.mode = this.modes.ADD;
    }
  }

  errorHandler(errorResponse) {
    this.loading = false;
    let error = errorResponse.error;
    if (error && error.errorId) {
      if (error.errorId === 1002) {
        this.serverError = `Specified ${error.description.field} already exists`;
      } else {
        this.serverError = `Error occured: ${error.message}`;
      }
    } else {
      throw new Error('Error occured at DocForm update');
    }
  }

  docSubmit(event) {
    this.loading = true;

    this.formDataIsComplete = true;
    this.serverError = false;
    this.childrenWidgets.forEach((item) => {
      let widgetValue = item.getValue();
      if (item.attr && item.attr.required && item.attr.required === false) {
        this.doc[item.attr.fieldname] = widgetValue;
      } else {
        if (widgetValue) {
          this.doc[item.attr.fieldname] = widgetValue;
        } else {
          this.formDataIsComplete = false;
        }
      }
    });

    if (this.formDataIsComplete) {
      if ('id' in this.doc && this.doc['id']!=-1){
        this.apiService.update(this.meta.dataset_id, this.doc).then(res => {
          this.loading = false;
          this.onSuccessfullSubmition.emit(res);
        }).catch(errorResponse => {
          this.errorHandler(errorResponse);
        });
      } else {
        this.apiService.add(this.meta.dataset_id, this.doc).then(res => {
          this.loading = false;
          this.onSuccessfullSubmition.emit(res);
        }).catch(errorResponse => {
          this.errorHandler(errorResponse);
        });
      }
    } else {
      this.loading = false;
    }
  }

  ngOnInit() {}

}
