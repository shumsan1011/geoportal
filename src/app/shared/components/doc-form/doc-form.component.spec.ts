import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocFormEmbeddedComponent } from './doc-form-embedded.component';

describe('DocFormEmbeddedComponent', () => {
  let component: DocFormEmbeddedComponent;
  let fixture: ComponentFixture<DocFormEmbeddedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocFormEmbeddedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocFormEmbeddedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
