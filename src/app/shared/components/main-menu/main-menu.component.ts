import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { APIService } from 'app/shared/services/api.service';
import { stateGetter } from 'app/store/app.reducer';

@Component({
    selector: '[main-menu]',
    templateUrl: './main-menu.component.html',
    styles: [
        `.menu-container {
      width: 100%;
      background-color: pink;
    }`,
        `nav .btn {
      color: #fff;
      border-color: #fff;
      cursor: pointer;
    }`,
        `nav .btn:hover {
      background-color: #3399ff;
    }`,
        `nav label {
      color: white;
      padding-right: 10px;
    }`,
        `.dropdown-toggle::after {
      content: none !important;
    }`
    ]
})
export class MainMenuComponent implements OnInit {
    model: any = {};

    currentUser: any;
    loading: Boolean = false;
    error: Boolean = false;
    modalReference: any;

    constructor(
        private modalService: NgbModal,
        private apiService: APIService,
        private store: Store<any>
    ) {
        this.store.select(stateGetter('user')).subscribe(value => {
            this.currentUser = value;
        });
    }

    ngOnInit() {}

    login() {
        this.loading = true;
        this.error = false;

        this.apiService.login(this.model.login, this.model.password).subscribe(
            data => {
                this.loading = false;
                this.error = true;
                this.modalReference.close();
            },
            error => {
                this.loading = false;
                this.error = true;
            }
        );
    }

    logout() {
        this.loading = false;
        this.error = false;

        this.apiService.logout();
    }

    open(content) {
        this.modalReference = this.modalService.open(content);
    }
}
