import { Component, ViewChild, Input, OnInit } from '@angular/core';
import { MapToolsAPIService } from "./../../../shared/api/maptools.api.service";
import { Store } from '@ngrx/store';
import { AppActions } from 'app/store/app.actions';
import { stateGetter } from 'app/store/app.reducer';
import * as wellknown from 'wellknown';
import * as L from 'leaflet';
import { setTimeout } from 'timers';
import { latLng, icon, geoJSON, marker, tileLayer, featureGroup } from 'leaflet';
import {NgForage, NgForageCache, NgForageConfig, CachedItem} from 'ngforage';
import { MapService } from 'app/shared/services/map.service';

const noDrawingControlSetting = {
  marker: false,
  polyline: false,
  polygon: false,
  circle: false,
  rectangle: false,
  circlemarker: false
};

@Component({
  template: '<div></div>',
})
export class MapComponent implements OnInit {
  static MODE_POINT = 0;
  static MODE_LINE = 1;
  static MODE_POLYGON = 2;

  mapId: string = '';

  featureGroup: any;
  drawOptions: any;
  drawingMode: Number = 0;
  drawingIsEnabled: Boolean = false;
  map: any;
  zoom: any;
  center: any;
  lastLayer: any;

  layersControl: any = {
    baseLayers: {
      'Open Cycle Map': tileLayer('http://{s}.tile.opencyclemap.org/{z}/{x}/{y}.png', { maxZoom: 18 }),
      'Open Street Map': tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18 })
    },
    overlays: {}
  };

  layers: Array<any> = [];

  layersControlOptions = { position: 'topright' };

	options = {
		zoom: 10,
		center: latLng(46.879966, -121.726909)
	};

  constructor(public mapTools: MapToolsAPIService, public mapService: MapService,
    public store: Store<any>, public actions: AppActions,
    readonly ngf: NgForage, readonly cache: NgForageCache) {
    this.featureGroup = featureGroup();
    this.lastLayer = false;
    this.drawOptions = {
      position: 'topright',
      draw: {}
   };
  }

  ngOnInit() {
    this.ngf.getItem(this.mapId).then(result => {
      if (result) {
        this.zoom = result['zoom'];
        this.center = result['center'];
      }
    });

    this.store.select(stateGetter('tables')).subscribe(tables => {
      for (let key in tables) {
        let tableId = key;
        let tableData = tables[key];

        let geomColumns = [];
        if (tableData.meta && tableData.meta.columns) {
          tableData.meta.columns.map(item => {
            if (['point', 'line', 'polygon'].indexOf(item.type) !== -1) {
              let unique = `${Math.round((new Date()).getTime() / 1000)}${Math.floor(Math.random() * 9999) + 1000}`;
              this.mapTools.createMAPFileForField(tableData.meta.dataset_id, `${unique}${item.fieldname}`, item.fieldname)
              .then(result => {
                let layer = L.tileLayer.wms(result['wms_link'], {
                  layers: result['wms_layer_name'] + ',selection',
                  format: 'image/gif',
                  transparent: true
                });

                this.layersControl.overlays[item.fieldname] = layer;
                this.layers.push(layer);
              });
            }
          });
        }
      }
    });
  }

  clearFeatures() {
    if (this.featureGroup) {
      this.featureGroup.eachLayer(layer => {
        this.map.removeLayer(layer);
      });
    }
  }

  clearDrawnFeatures() {
    setTimeout(() => {
      if (this.drawOptions.edit) {
        this.drawOptions.edit.featureGroup.removeLayer(this.lastLayer);
      }
    }, 100);
  }

  convertToMulti(data) {
    if (data.geometry && data.geometry.type) {
      switch (data.geometry.type) {
        case 'Point':
          data.geometry.type = 'MultiPoint';
          data.geometry.coordinates = [data.geometry.coordinates];
          break;
        default:
          throw new Error('Not implemented yet');
      }
    } else {
      throw new Error('Invalid GeoJSON');
    }

    return data;
  }

  onMapReady(event) {
    let _self = this;

    this.map = event;

    const storeCurrentMapCenterAndZoom = (event) => {
      this.ngf.setItem(this.mapId, {
        zoom: event.target.getZoom(),
        center: event.target.getCenter()
      });
    };

    this.map.on('zoomend', storeCurrentMapCenterAndZoom);
    this.map.on('moveend', storeCurrentMapCenterAndZoom);

    console.log(this.map);

    this.map.on('draw:created', (data) => {
      let converted = this.convertToMulti(data.layer.toGeoJSON());

      this.lastLayer = data.layer;
      let wktRepresentation = '';
      switch (this.drawingMode) {
        case this.constructor['MODE_POINT']:       
          this.actions.setLastPointForMap({
            mapId: this.mapId,
            wkt: wellknown.stringify(converted)
          });
          break;
        case this.constructor['MODE_LINE']:
          this.actions.setLastLineForMap({
            mapId: this.mapId,
            wkt: wellknown.stringify(converted)
          });
          break;
        case this.constructor['MODE_POLYGON']:
          this.actions.setLastPolygonForMap({
            mapId: this.mapId,
            wkt: wellknown.stringify(converted)
          });
          break;
      }

      this.drawingIsEnabled = false;
    });

    //L.control.layers(this.baseLayers, {}).addTo(this.map);
    this.map.addLayer(tileLayer('http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png'));

    this.actions.addMap({ mapId: this.mapId });
    this.store.select(stateGetter('maps', this.mapId)).subscribe(value => {
      this.clearFeatures();
      this.clearDrawnFeatures();

      if (this.drawingIsEnabled !== value['drawingIsEnabled']) {
        let newDrawOptions = Object.assign({}, this.drawOptions);
        newDrawOptions.draw = Object.assign({}, noDrawingControlSetting);
        if (value['existingFeature']) {
          let parsedFeature = wellknown.parse(value['existingFeature']);
          let newLayer = geoJSON(parsedFeature, {
            pointToLayer: (feature, latlng) => {
              return marker(latlng, {
                icon: icon({
                  iconSize: [25, 41],
                  iconAnchor: [13, 41],
                  iconUrl: 'assets/marker-icon-red.png',
                  shadowUrl: 'assets/marker-shadow.png'
                })
              });
            },
            style: () => {
              return {
                weight: 3,
                opacity: 1,
                color: '#FF3333',
                fillOpacity: 0.3,
                fillColor: '#FF3333'
              };
            }
          });

          this.featureGroup.addLayer(newLayer);
          this.featureGroup.addTo(this.map);
        }

        switch (value['drawingMode']) {
          case this.constructor['MODE_POINT']:
            newDrawOptions.draw.marker = {
              icon: icon({
                iconSize: [25, 41],
                iconAnchor: [13, 41],
                iconUrl: 'assets/marker-icon.png',
                shadowUrl: 'assets/marker-shadow.png'
              })
            };
            break;
          case this.constructor['MODE_LINE']:
            newDrawOptions.draw.polyline = true;
            break;
          case this.constructor['MODE_POLYGON']:
            newDrawOptions.draw.polygon = true;
            break;
        }

        this.drawingMode = value['drawingMode'];
        this.drawOptions = newDrawOptions;
        this.drawingIsEnabled = value['drawingIsEnabled'];
      }
    });
  }
}
