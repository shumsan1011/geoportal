import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/filter';
import {
  NgbModal,
  NgbModalRef,
  NgbTabChangeEvent
} from '@ng-bootstrap/ng-bootstrap';
import { KeysPipe, ValuesPipe } from 'ngx-pipes';
import { cloneDeep, filter, forOwn } from 'lodash';
import { Promise } from 'q';

import { TableDlgComponent } from 'app/shared/components/table-dlg/table-dlg.component';
import { TableModelComponent } from 'app/shared/components/table-model/table-model.component';
import { AppState, DataTableState, TableContext } from 'app/store/app.state';
import { AppActions } from 'app/store/app.actions';
import { stateGetter } from 'app/store/app.reducer';
import { DocFormComponent } from 'app/shared/components/doc-form/doc-form.component';
import { APIService } from 'app/shared/services/api.service';
import { ModalResult } from 'app/shared/interfaces';

@Component({
  selector: 'gp-tablelist',
  templateUrl: './tablelist.component.html',
  styles: ['.header-buttons { margin-left: auto; }'],
  providers: [KeysPipe, ValuesPipe]
})
export class TablelistComponent implements OnInit, OnDestroy {
  // private tableList$: Observable<DataTableState[]>;
  // private tableList: DataTableState[];
  public datasetList: string[] = [];
  private subscription;

  constructor(
    private store: Store<AppState>,
    private appActions: AppActions,
    private datatableService: APIService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    // this.tableList$ = this.store
    //   .select(stateGetter('tables'))
    //   .map(tables => filter(tables, table => table.context === TableContext.TableList));
    this.subscription = this.store.select(stateGetter('tables'))
      .map(tables => filter(tables, table => table.context === TableContext.TableList))
      .subscribe(filteredTablesArray => {
        filteredTablesArray.forEach(table => {
          if (!this.datasetList.includes(table.dataset_id)) {
            this.datasetList.push(table.dataset_id);
          }
        });
        // this.tableList = filteredTables;
        // console.log('New table List recived', this.datasetList);
      });
    console.log('Subsribed. Table List');

    this.appActions.showTable(20, 1);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    console.log('Unsubsribed. Table List');
  }

  /**
   * Открытие диалога добавления новой таблицы
   * TODO получить из диалога данные и уже здесь сделать this.appActions.showTable
   */
  opentab() {
    const modalRef = this.modalService.open(TableDlgComponent);
    modalRef.result.then(result => {
      if (result.choosed === ModalResult.mrOk) {
        this.appActions.showTable(result.dataset_id, TableContext.TableList );
      }
    });
  }

  openmodel(){
    const modalRef = this.modalService.open(TableModelComponent, {size: 'lg'});
  }

  public onTabChange($event: NgbTabChangeEvent) {
    console.log(`Выбрана вкладка id = ${$event.nextId}`);
    if ($event.nextId === 'tab-preventchange2') {
      $event.preventDefault();
    }
  }
}
