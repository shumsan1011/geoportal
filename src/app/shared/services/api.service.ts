/**
 * @todo  * Got to keep the average number of lines in class around 200, so
 * class needs to be splitted in minor ones.
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { State, Store } from '@ngrx/store';
import 'rxjs/add/operator/map';

import { AppConfig } from 'app/app.config';
import { AppActions } from 'app/store/app.actions';
import { AppState } from 'app/store/app.state';
import {
  Metadata,
  MetadataFilter,
  AuthenticationReply,
  SortingField,
  MetadataSorting
} from 'app/shared/interfaces';

@Injectable()
export class APIService {
  constructor(private http: HttpClient, private appActions: AppActions) {}

  checkAuthentication() {
    const tokens = localStorage.getItem('geoportal_tokens');
    if (tokens && tokens !== 'null') {
      this.appActions.setUser(JSON.parse(tokens));
    }
  }

  login(username: string, password: string) {
    const res = this.http.post(`${AppConfig.APIPath}/auth/login`, {
      username,
      password
    });

    res.subscribe(data => {
      const userData: AuthenticationReply = {
        accessToken: data['accessToken'],
        refreshToken: data['refreshToken']
      };

      localStorage.setItem('geoportal_tokens', JSON.stringify(data));
      this.appActions.setUser(userData);
    });

    return res;
  }

  logout() {
    localStorage.setItem('geoportal_tokens', null);
    this.appActions.unsetUser();
  }

  /**
   * Returns metainformation for the specified table.
   * 
   * @param dataTableId Table identifier
   */
  meta(dataTableId: number) {
    const params = new HttpParams().set('f', String(100)).set('f_id', String(dataTableId));
    return this.http.get(`${AppConfig.APIPath}/geoThemes/dataset/list`, { params }).toPromise().then(res => {
      let result = new Promise((resolve, reject) => {
        if (res['aaData'] && res['aaData'].length === 1) {
          const s = res['aaData'][0]['JSON'];
          resolve(JSON.parse(s));
        } else {
          reject('Unable to retrieve the table data');
        }
      });

      return result;
    });
  }

  /**
   * @param dataTableId  table identifier
   * @param limit row count
   * @param offset for pagination
   * @param filter Если не null, то формируем фильтр и запрашиваем данные
   * @see https://www.typescriptlang.org/docs/handbook/declaration-files/do-s-and-don-ts.html
   */
  list(
    dataTableId: number,
    limit: number,
    offset: number,
    filters?: MetadataFilter[],
    sorting?: MetadataSorting[]
  ) {
    const url = AppConfig.APIPath + '/geoThemes/dataset/list';
    let params = new HttpParams().set('f', String(dataTableId));
    if (filters) {
      filters.forEach(filter => {
        params = params.set(
          'f_' + filter.fieldname,
          filter.valueList.join(';')
        );
      });
    }

    if (sorting) {
      const strangeSortingArray = []
      sorting.forEach(sortingColumn => {
        strangeSortingArray.push({
          fieldname: sortingColumn.fieldname,
          dir: sortingColumn.sorting.direction
        });
      });

      const jsonSorting = JSON.stringify(strangeSortingArray);
      params = params.set('sort', jsonSorting);
    }

    params = params.set('count_rows', 'true');
    params = params.set('iDisplayStart', String(offset));
    params = params.set('iDisplayLength', String(limit));

    return this.http.get(url, { params }).toPromise();
  }


  listObservable(
    dataTableId: number,
    limit: number,
    offset: number,
    filters?: MetadataFilter[],
    sort?: SortingField[]
  ) {
    const url = AppConfig.APIPath + '/geoThemes/dataset/list';
    let params = new HttpParams().set('f', String(dataTableId));
    if (filters) {
      for (let filter of  filters) {
        params = params.set(
          'f_' + filter['fieldname'],
          filter['valueList'].join(';')
        );
      }
    }
    params = params.set('count_rows', 'true');

    if (sort) {
      const sortjson = JSON.stringify(sort);
      params = params.set('sort', sortjson);
    }
    params = params.set('iDisplayStart', String(offset));
    params = params.set('iDisplayLength', String(limit));

    return this.http.get(url, { params });
  }
  /**
   * Adding row in dataset
   *
   * @param dataTableId {number} dataset identifier
   *
   * @param document {object} new data
   *
   * @return {Promise}
   */
 
  add(datasetId: number, document) {
    const url = AppConfig.APIPath + '/geoThemes/dataset/add?f=' + String(datasetId);
    document.dataset_id = datasetId;

    const documentjson = JSON.stringify(document);

    return this.http
      .post(url, { f: datasetId, document: documentjson })
      .toPromise();
  }

  /**
   * Updating row in dataset
   *
   * @param dataTableId {number} dataset identifier
   *
   * @param document {object} updated data
   *
   * @return {Promise}
   */
  update(dataTableId: number, document) {
    const url =
      AppConfig.APIPath + '/geoThemes/dataset/update?f=' + String(dataTableId);
    document.dataset_id = dataTableId;
    document.f_id = document.id;

    const documentjson = JSON.stringify(document);

    return this.http
      .post(url, { f_id: document.id, document: documentjson })
      .toPromise();
  }

  /**
   * Deleting row in dataset
   *
   * @param dataTableId {number} dataset identifier
   *
   * @param document {object} deleted data
   *
   * @return {Promise}
   */
  delete(dataTableId: number, document) {
    const url =
      AppConfig.APIPath + '/geoThemes/dataset/delete?f=' + String(dataTableId);
    document.dataset_id = dataTableId;
    document.f_id = document.id;

    const documentjson = JSON.stringify(document);

    return this.http
      .post(url, { f_id: document.id, document: documentjson })
      .toPromise();
  }



    /**
   * Creating dataset
   *
   *
   * @param document {object} deleted data
   *
   * @return {Promise}
   */
  createdatatable(meta: Metadata) {
    const url =
      AppConfig.APIPath + '/geoThemes/processtheme';
    //let params = new HttpParams()
    //  .set('f', String(dataTableId));

    let tablejson = JSON.stringify(meta);
    //params = params.set('document', documentjson);

    return this.http
      .post(url, { tablejson: tablejson })
      .toPromise();
  }
  /**
   * Registers or updates the service or scenarion in the catalog
   *
   * @param serviceInformation {Object} Item information
   *
   * @return {Promise}
   */
  sendCatalogItem(serviceInformation) {
    return this.http
      .post(`${AppConfig.APIPath}/servicesManager/create`, {
        data: JSON.stringify(serviceInformation)
      })
      .toPromise();
  }

  /**
   * Fetches the specified catalog item
   *
   * @param id {Number} Catalog item identifier
   */
  getCatalogItem(id: Number) {
    return this.http
      .get(`${AppConfig.APIPath}/servicesManager/getinfo?id=${id}`)
      .toPromise();
  }

  /**
   * Fetches the console output for the specific catalog item run
   *
   * @param runId {Number} Identifier of the catalog item run
   */
  getCatalogItemRunConsoleOutput(runId: Number) {
    return this.http
      .get(`${AppConfig.APIPath}/servicesManager/consoleoutput?id=${runId}`)
      .toPromise();
  }

  /**
   * Runs the catalog item with specified parameters
   *
   * @param id               {Number} Identifier of the catalog item
   * @param inputParameters  {Object} Input parameters values
   * @param outputParameters {Object} Output parameters values
   */
  runCatalogItem(
    id: Number,
    inputParameters: Object,
    outputParameters: Object
  ) {
    return this.http
      .get(
        `${
          AppConfig.APIPath
        }/servicesManager/execute?id=${id}&inputparams=${encodeURIComponent(
          JSON.stringify(inputParameters)
        )}&outputparams=${encodeURIComponent(JSON.stringify(outputParameters))}`
      )
      .toPromise();
  }

  /**
   * Proxifies the request to the external server (avoiding CORS issues).
   *
   * @param requestType {String} The request type ('GET')
   * @param url         {String} Requested URL address
   *
   * @return {Promise}
   */
  proxifyRequest(requestType: string, url: string): Promise<any> {
    const completeProxyUrl = `${
      AppConfig.APIPath
    }/servicesManager/proxy?url=${encodeURIComponent(url)}`;
    if (['GET'].indexOf(requestType) === -1) {
      throw new Error(`Invalid parameter`);
    }
    return this.http
      .get(completeProxyUrl, { responseType: 'text' })
      .toPromise();
  }

  /**
   * Activates user account
   * 
   * @param code {String} Activation code
   */
  activateUser(code: string) {
    return this.http.get(`${AppConfig.APIPath}/users/activate?code=${code}`).toPromise();
  }
}
