import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

interface WidgetValue {
  fieldname: string;
  value: string;
}

export class fieldval implements WidgetValue {
  constructor(
    public fieldname: string,
    public value: string
  ) { }
}

@Injectable()
export class DocService {
  // Observable string sources
  private widgets = new Subject<WidgetValue>();
  private form = new Subject<WidgetValue>();

  // Observable string streams
  widgets$ = this.widgets.asObservable();
  formSource$ = this.form.asObservable();

  // Service message commands
  confirmWidget(val: WidgetValue) {
    this.widgets.next(val);
  }

  confirmForm(value: WidgetValue) {
  // alert(value.fieldname+'form catch');
    this.form.next(value);
  }
}
