import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { Ng2CompleterModule } from 'ng2-completer';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

import { WidgetComponent } from './components/widget/widget.component';
import { WEditComponent } from './components/w-edit/w-edit.component';
import { WPasswordComponent } from './components/w-password/w-password.component';
import { WDateComponent } from './components/w-date/w-date.component';
import { WGeometryComponent } from './components/w-geometry/w-geometry.component';
import { WPointComponent } from './components/w-point/w-point.component';
import { WLineComponent } from './components/w-line/w-line.component';
import { WPolygonComponent } from './components/w-polygon/w-polygon.component';
import { WTextareaComponent } from './components/w-textarea/w-textarea.component';
import { WNumberComponent } from './components/w-number/w-number.component';
import { WBooleanComponent } from './components/w-boolean/w-boolean.component';
import { WTableComponent } from './components/w-table/w-table.component';
import { WSelectComponent } from './components/w-select/w-select.component';
import { WClassifyComponent } from './components/w-classify/w-classify.component';

import { SignComponent } from 'app/shared/modules/widgets/components/sign/sign.component';
import { WFilterComponent } from 'app/shared/modules/widgets/components/filter/filter.component';
import { FNumberComponent } from 'app/shared/modules/widgets/components/f-number/f-number.component';
import { FEditComponent } from 'app/shared/modules/widgets/components/f-edit/f-edit.component';
import { FDateComponent } from 'app/shared/modules/widgets/components/f-date/f-date.component';
import { DatatableComponent } from 'app/shared/modules/widgets/components/datatable/datatable.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        Ng2CompleterModule,
        NgbModule,
        NgbTooltipModule
    ],
    declarations: [
        WidgetComponent,
        WEditComponent,
        WPasswordComponent,
        WDateComponent,
        WGeometryComponent,
        WPointComponent,
        WLineComponent,
        WPolygonComponent,
        WTextareaComponent,
        WNumberComponent,
        WBooleanComponent,
        WTableComponent,
        WSelectComponent,
        WClassifyComponent,
        FDateComponent,
        FEditComponent,
        FNumberComponent,
        SignComponent,
        WFilterComponent,
        DatatableComponent
    ],
    exports: [
        WidgetComponent,
        WFilterComponent,
        WTableComponent,
        DatatableComponent
    ]
})
export class WidgetsModule {}
