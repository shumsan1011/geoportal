import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WBooleanComponent } from './w-boolean.component';

describe('WBooleanComponent', () => {
  let component: WBooleanComponent;
  let fixture: ComponentFixture<WBooleanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WBooleanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WBooleanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
