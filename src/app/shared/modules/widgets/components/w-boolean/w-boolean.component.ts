import { Component, OnInit, Input, Output, OnDestroy, EventEmitter } from '@angular/core';
import { Subscription }   from 'rxjs/Subscription';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'w-boolean',
  templateUrl: './w-boolean.component.html',
  styleUrls: ['./w-boolean.component.css']
})
export class WBooleanComponent {

  @Input() attr={};
  @Input() mode="input";
  values=[{name:'Yes', value:'true'}, {name:'No', value:'false'},{name:'Unknown'}];

  //@Input() onChange;
  @Input() widgetValue='';
  @Output() widgetValueChange: EventEmitter<any>;

  constructor() {
    this.widgetValueChange = new EventEmitter<any>();
  }

  getvalue(){
    return (this.widgetValue);
  }

  change(val) {
    this.widgetValue = val;
    this.widgetValueChange.emit([null, this.widgetValue, this]);
  }

}
