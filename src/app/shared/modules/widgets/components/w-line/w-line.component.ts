import { Component, ElementRef, Input, Output, OnInit, OnDestroy, EventEmitter, HostListener } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { DocService, fieldval } from 'app/shared/services/doc.service';
import { FormsModule } from '@angular/forms';
import { MetadataColumns } from 'app/shared/interfaces';
import { WGeometryComponent } from '../w-geometry/w-geometry.component';
import { wkt2decimal } from 'app/shared/conversions';
import { AppActions } from 'app/store/app.actions';
import { stateGetter } from 'app/store/app.reducer';
import { Store } from '@ngrx/store';
import { MapComponent } from 'app/shared/components/map/map.component';

@Component({
  moduleId: module.id,
  selector: 'w-line',
  templateUrl: '../w-geometry/w-geometry.component.html',
  host: {
    '(document:click)': 'onClick($event)',
  },
  providers: [DocService]
})
export class WLineComponent extends WGeometryComponent {
  private featureName: string = 'lastLine';

  constructor(public docService: DocService, public store: Store<any>, public _el: ElementRef, public actions: AppActions) {
    super(docService, store, _el, actions);
  }

  enableFormInput() {
    this.editingIsEnabled = true;
    this.subscription = this.store.select(stateGetter('maps', this.mapId)).subscribe(mapData => {
      if (this.editingIsEnabled && this._mode === 'forminput' && mapData[this.featureName]) {
        this._ownValue = mapData[this.featureName];
        this.widgetValueChange.emit(this._ownValue);
        this.stopDrawing();
      }
    });

    this.actions.enableDrawingForMap({
      mapId: this.mapId,
      mode: MapComponent.MODE_LINE,
      existingFeature: ((this._ownValue) ? this._ownValue : false)
    });
  }

  get ownValue(): string {
    if (this._ownValue) {
      return `${this._ownValue.substring(0, 8)}...`;
    } else {
      return 'Empty';
    }
  }
}
