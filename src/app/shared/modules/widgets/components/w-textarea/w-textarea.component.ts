import { Component, OnInit, Input, Output, OnDestroy, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'w-textarea',
  templateUrl: './w-textarea.component.html',
  styleUrls: ['./w-textarea.component.css']
})

export class WTextareaComponent {

  @Input() attr = {};
  @Input() mode = "input";

  // @Input() onChange;
  @Input() widgetValue = '';
  @Output() widgetValueChange: EventEmitter<any>;

  constructor() {
    this.widgetValueChange = new EventEmitter<any>();
  }

  getvalue() {
    return (this.widgetValue);
  }

  onBlur(event) {
    // this.widgetValue = this.getvalue();
    this.widgetValueChange.emit([event, this.widgetValue, this]);
  }



}
