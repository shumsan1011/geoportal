import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WTextareaComponent } from './w-textarea.component';

describe('WTextareaComponent', () => {
  let component: WTextareaComponent;
  let fixture: ComponentFixture<WTextareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WTextareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WTextareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
