import {
    Component,
    Input,
    OnInit,
    Output,
    EventEmitter,
    HostListener
} from '@angular/core';
import { MetadataColumns, DataDocument } from 'app/shared/interfaces';

@Component({
    selector: 'w-table',
    templateUrl: './w-table.component.html',
    styleUrls: ['./w-table.component.scss']
})
export class WTableComponent implements OnInit {
    @Input() mode = 'forminput';
    @Output()
    widgetValueChange: EventEmitter<string> = new EventEmitter<string>();

    private _widgetValue = '';

    @Input()
    set widgetValue(value: string) {
        this._widgetValue = value;
    }

    get widgetValue(): string {
        return this._widgetValue;
    }

    constructor() {}

    ngOnInit() {}

    changetab(tab: DataDocument) {
        this.widgetValueChange.emit(String(tab.id));
        console.log(tab);
    }
}
