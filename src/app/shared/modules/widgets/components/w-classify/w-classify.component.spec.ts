import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WClassifyComponent } from './w-classify.component';

describe('WClassifyComponent', () => {
  let component: WClassifyComponent;
  let fixture: ComponentFixture<WClassifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WClassifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WClassifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
