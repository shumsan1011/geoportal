import {
  Component,
  Input,
  Output,
  OnInit,
  EventEmitter,
  HostListener
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  MetadataColumns,
  MetadataFilter,
  Metadata
} from 'app/shared/interfaces';
import { Ng2CompleterModule } from 'ng2-completer';
import { APIService } from 'app/shared/services/api.service';
import { CompleterSearch } from 'app/shared/modules/widgets/components/w-edit/CompleterSearch';

@Component({
  selector: 'w-classify',
  templateUrl: './w-classify.component.html',
  styleUrls: ['./w-classify.component.scss']
})
export class WClassifyComponent implements OnInit {
  @Input() attr: MetadataColumns;
  @Input() mode = 'input';
  @Output()
  widgetValueChange: EventEmitter<string> = new EventEmitter<string>();

  public customData: CompleterSearch;

  private _widgetIdValue = '';
  public strwidgetValue = '';
  private refMeta: Metadata = {};
  private _objectValue={};

  @Input()
  set widgetValue(value) {
    this._objectValue=value;    
  }

  get widgetValue(): string {
    return this._widgetIdValue;
  }

  getValue() {
    return this._widgetIdValue;
  }

  constructor(private datatableService: APIService) {}

  ngOnInit() {
    if (!this.attr)
      this.attr = {
        fieldname: '',
        tablename: '',
        sqltablename: '',
        sqlname: '',
        widget: { name: '', properties: {} }
      };
    if (!this.attr.widget) this.attr.widget = { name: '', properties: {} };
    if (this.attr.widget.properties.dataset_id) {
      this.customData = new CompleterSearch(
        this.datatableService,
        this.attr.widget.properties.dataset_id,
        this.attr.widget.properties.db_field.join(',')
      );
    }
    if (!this.attr.widget.properties.db_field)
      this.attr.widget.properties.db_field = [];
    this.changedataset(this.attr.widget.properties.dataset_id);
    this.attr.widget.properties.reftablename =
      'r_' +
      Math.random()
        .toString(36)
        .substring(2, 10);
    if (typeof this._objectValue === 'object') {
      this._widgetIdValue = this._objectValue['id'];
      this.strwidgetValue = '';
      this.attr.widget.properties.db_field.forEach(element => {
        this.strwidgetValue += ' ' + this._objectValue[element];
      });

    } else this._widgetIdValue = this._objectValue;

    if (this.strwidgetValue == '' && this._widgetIdValue !== '') {
      let filters: MetadataFilter = {
        fieldname: 'id',
        valueList: [this._widgetIdValue]
      };
      this.datatableService
        .list(this.attr.widget.properties.dataset_id, 1, 0, [filters])
        .then(rows => {
          this.strwidgetValue = '';
          this.attr.widget.properties.db_field.forEach(element => {
            this.strwidgetValue += ' ' + rows['aaData'][0][element];
          });

          // this.collectionSize=100;
        });
    }
  }


  widgetValueSubmitted(event) {
    //if (this.widgetValue) {
    this._widgetIdValue = event.originalObject;
    this.widgetValueChange.emit(event.originalObject);
    //}
  }

  onChange($event) {
    this.widgetValueSubmitted(event);
  }

  setcontrol_type(value) {
    this.attr.widget.properties.control_type = value;
  }

  changedataset(dataset_id) {
    if (!dataset_id) return;
    console.log(dataset_id);
    this.attr.widget.properties.reftablename =
      'r_' +
      Math.random()
        .toString(36)
        .substring(2, 10);
    this.attr.widget.properties.dataset_id = dataset_id;

    this.datatableService.meta(dataset_id).then(serviceMeta => {
      this.refMeta = serviceMeta;
      if (!this.attr.widget.properties.db_field) {
        this.attr.widget.properties.db_field = [];
      }

      this.refMeta.columns.forEach(element => {
        element['checked'] =
          this.attr.widget.properties.db_field.indexOf(element.fieldname) !==
          -1;
      });
    });
  }

  changeRefColumn(col) {
    let index = this.attr.widget.properties.db_field.indexOf(col.fieldname);

    if (col.checked) {
      this.attr.widget.properties.db_field.splice(index);
    } else {
      this.attr.widget.properties.db_field.push(col.fieldname);
    }
    col.checked = !col.checked;
  }
}
