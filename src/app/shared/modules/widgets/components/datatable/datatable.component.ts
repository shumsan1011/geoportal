import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';

import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep, isEqual } from 'lodash';

import {
  MetadataFilter,
  SortingField,
  Metadata,
  DataDocument,
  ModalResult,
  MetadataColumns,
  MetadataSorting
} from 'app/shared/interfaces';
import { APIService } from 'app/shared/services/api.service';
import { DocFormTableEditingWrapperComponent } from 'app/shared/components/doc-form-table-editing-wrapper/doc-form-table-editing-wrapper.component';
import { AppState, DataTableState, TableContext } from 'app/store/app.state';
import { AppActions } from 'app/store/app.actions';
import { stateGetter } from 'app/store/app.reducer';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ModalConfirmationComponent } from 'app/shared/components/modal-confirmation/modal-confirmation.component';

@Component({
  selector: 'gp-datatable',
  templateUrl: 'datatable.component.html',
  styleUrls: ['datatable.component.scss']
})
export class DatatableComponent implements OnInit, OnDestroy {
  @Input() mode = '';
  @Input() datasetId: number;
  @Input() context: TableContext = TableContext.ServiceTables;
  @Output() curenttable: EventEmitter<DataDocument> = new EventEmitter<DataDocument>();

  // selectedRowKeys = []; лучше сделать массив, и в нем хранить ключи. подсмотрено у devexpress datagrid
  currentRow: DataDocument = { id: -1 };
  closeResult: string;

  public table: DataTableState; // Должен быть главным
  private subscription;
  private lastTableState: DataTableState;

  constructor(
    private modalService: NgbModal,
    private datatableService: APIService,
    private store: Store<AppState>,
    private appActions: AppActions
  ) { }

  private initTableState(): void {
    this.table = {
      dataset_id: this.datasetId,
      showFilteringBar: false,
      showAggregationBar: false,
      context: this.context
    };
  }

  openModalForm() {
    const modalRef = this.modalService.open(DocFormTableEditingWrapperComponent, {
      size: 'lg',
      backdrop: 'static'
    });

    modalRef.componentInstance.meta = this.table.meta;
    modalRef.componentInstance.doc = this.currentRow;
  }

  /**
   * Еще внутрь API имеет смысл добавить ДЕЙСТВИЕ isFetching для показа спиннера загрузки
   */
  ngOnInit() {
    this.initTableState();
    if (this.table.meta === undefined) {
      this.getMetadataFromAPIandInitSettings(this.table).then(
        initializedTable => {
          this.appActions.setMetadataForTable(
            initializedTable.dataset_id,
            initializedTable.meta
          );
          this.subscription = this.store
            .select(stateGetter('tables', this.table.dataset_id))
            .subscribe(choosedTable => {
              this.table = { ...this.table, ...cloneDeep(choosedTable) };
              this.getDataFromAPI(this.table).then(tableWithData => {
                this.table = tableWithData;
                console.log('Data recieved for table =', this.table.dataset_id);
              });
            });
          console.log('Subsribed. Dataset ID = ', this.table.dataset_id);
        }
      );
    }
  }

  ngOnDestroy() {
    console.log('Unsubsribed. Dataset ID = ', this.table.dataset_id);
    if (this.subscription) this.subscription.unsubscribe();
  }

  onChangeFilter(filter: MetadataFilter) {
    console.log("NEW FILTER", filter);
    this.appActions.setTableFilter(this.table.dataset_id, filter);
  }

  onPageChanged(page: number) {
    if (isNaN(page) || page === 0) {
      return;
    }
    this.appActions.setPaginatorPageForTable(this.table.dataset_id, page);
  }

  changePageSize(pageSize: number) {
    this.appActions.setPaginatorPageSizeForTable(
      this.table.dataset_id,
      pageSize
    );
  }

  changeOrder(column: MetadataColumns) { // changeColumnSortingDirection
    const sortingColumn: MetadataSorting = {
      fieldname: column.fieldname,
      sorting: {
        direction: '',
        className: 'fa-sort'
      }
    };

    if (column.sortingOrder.direction === '') {
      sortingColumn.sorting.direction = 'ASC';
      sortingColumn.sorting.className = 'fa-sort-asc';
    } else if (column.sortingOrder.direction === 'ASC') {
      sortingColumn.sorting.direction = 'DESC';
      sortingColumn.sorting.className = 'fa-sort-desc';
    } else if (column.sortingOrder.direction === 'DESC') {
      // сбросить состояние сортировки
    }
    this.appActions.setSortingForTableColumn(this.datasetId, sortingColumn);
  }

  editRow(row) {
    this.currentRow = row;
    this.openModalForm();
  }

  addRow() {
    this.currentRow = { id: -1 };
    this.openModalForm();
  }

  onRowClicked(row) {
    this.currentRow = row;
    this.curenttable.emit(this.currentRow);
  }

  openConfirmationModalForm(
    title: String = 'Geoportal',
    body: String
  ): Promise<any> {
    const modalRef = this.modalService.open(ModalConfirmationComponent);
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.body = body;
    return modalRef.result;
  }

  deleteRow(row) {
    // row: table.data.aaData
    this.currentRow = row;
    this.openConfirmationModalForm(
      'Deleting row',
      'Are you sure you want to delete this row?'
    ).then(result => {
      if (result === ModalResult.mrYes) {
        // const doc = { id: this.currentRow.id };
        // this.datatableService.delete(this.table.meta.dataset_id, doc).then(res => {
        //   console.log(res);
        //   this.appActions.updateTable(this.table.meta.dataset_id);
        // });
        console.log(
          `Внутри datatable получили Да ,удалить ${this.currentRow['id']}`
        );
      }
    });
  }

  showTable(): boolean {
    if (this.table.meta !== undefined) {
      return true;
    } else {
      return false;
    }
  }

  showFiltering(): boolean {
    if (this.table.showFilteringBar && this.table.meta !== undefined) {
      return true;
    } else {
      return false;
    }
  }

  showAggregation(): boolean {
    if (this.table.showAggregationBar && this.table.meta !== undefined) {
      return true;
    } else {
      return false;
    }
  }

  toggleFilteringBarVisible() {
    if (this.table.showFilteringBar) {
      this.appActions.setFilteringBarVisibilty(this.table.dataset_id, false);
    } else {
      this.appActions.setFilteringBarVisibilty(this.table.dataset_id, true);
    }
  }

  toggleAggregationBarVisible() {
    if (this.table.showAggregationBar) {
      this.appActions.setAggregationBarVisibilty(this.table.dataset_id, false);
    } else {
      this.appActions.setAggregationBarVisibilty(this.table.dataset_id, true);
    }
  }

  getDataFromAPI(dataSet: DataTableState): Promise<DataTableState> {
    const filtersToAPI: MetadataFilter[] = [];
    this.table.meta.columns.forEach(column => {
      const filter: MetadataFilter = {
        fieldname: column.fieldname,
        valueList: column.filterValueList
      };
      if (filter.valueList.length > 0) {
        filtersToAPI.push(filter);
      }
    });

    const sortingToAPI: MetadataSorting[] = [];
    this.table.meta.columns.forEach(column => {
      const sortingColumn: MetadataSorting = {
        fieldname: column.fieldname,
        sorting: column.sortingOrder
      };
      if (sortingColumn.sorting.direction !== '') {
        sortingToAPI.push(sortingColumn);
      }
    });

    const promise = new Promise<DataTableState>((resolve, reject) => {
      this.datatableService
        .list(
          dataSet.dataset_id,
          dataSet.pageSize,
          dataSet.pageSize * (dataSet.page - 1), // следует ставить = 0
          filtersToAPI,
          sortingToAPI
        )
        .then(rows => {
          dataSet.data = rows;
          dataSet.collectionSize = rows['iTotalRecords'];
          resolve(dataSet);
        });
    });
    return promise;
  }

  getMetadataFromAPIandInitSettings(
    dataSet: DataTableState
  ): Promise<DataTableState> {
    const promise = new Promise<DataTableState>((resolve, reject) => {
      this.datatableService
        .meta(dataSet.dataset_id)
        .then((serviceMetadata: Metadata) => {
          dataSet.meta = serviceMetadata;
          dataSet.meta.columns.forEach(column => {
            column.sortingOrder = {
              className: 'fa-sort',
              direction: ''
            };
            column.filterValueList = [];
          });
          dataSet.page = 1; // pagination from first page
          dataSet.pageSize = 5; // pagination Size for first page
          dataSet.collectionSize = 0;
          dataSet.showFilteringBar = false;
          dataSet.showAggregationBar = false;
          resolve(dataSet);
        });
    });
    return promise;
  }
}
