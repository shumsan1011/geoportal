import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild
} from '@angular/core';
import { MetadataColumns, MetadataFilter } from 'app/shared/interfaces';
import { WEditComponent } from 'app/shared/modules/widgets/components/w-edit/w-edit.component';

@Component({
  moduleId: module.id,
  selector: 'f-edit',
  templateUrl: 'f-edit.component.html',
  styleUrls: ['f-edit.component.scss']
})
export class FEditComponent {
  @Input() column: MetadataColumns;
  @Input() mode = {};
  @Input() widgetValue: MetadataFilter = { fieldname: '', valueList: [] };
  // @see https://angular.io/guide/styleguide#dont-prefix-output-properties
  @Output() valueChange = new EventEmitter<MetadataFilter>(); // объект генерит события
  public currentVal = '';

  @ViewChild(WEditComponent) private widget: WEditComponent;

  notifyParent() {
    /*
    const filter: MetadataFilter = {
      fieldname: this.attr.fieldname,
      fieldlist: this.widgetValue
    };*/
    this.widgetValue.fieldname = this.column.fieldname;
    this.widgetValue.valueList = this.column.filterValueList;

    this.valueChange.emit(this.widgetValue);
  }

  public addItem(item: string) {
    if (this.column.filterValueList.indexOf(item) === -1) {
      this.column.filterValueList.push(item);
    }
    this.widget.widgetValue = '';
    this.notifyParent();
  }

  private removeItem(item: string) {
    const index = this.column.filterValueList.indexOf(item, 0);
    if (index > -1) {
      this.column.filterValueList.splice(index, 1);
    }
    this.notifyParent();
  }
}
