import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'widget',
  templateUrl: './widget.component.html',
})
export class WidgetComponent implements OnInit {
  @Input() attr= <any>{};
  @Input() widgetValue = '';
  @Input() mode= {};
  @Output() valueChange: EventEmitter<string>;

  ngOnInit() {}

  getValue() {
    return  this.widgetValue;
  }

  changeval(mas) {
    this.widgetValue = mas;

    /*
    this.valueChange.emit(this.widgetValue);*/
  }
}
