import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DocService, fieldval } from 'app/shared/services/doc.service';
import { MetadataColumns } from 'app/shared/interfaces';

@Component({
  selector: 'w-number',
  templateUrl: './w-number.component.html',
  styleUrls: ['./w-number.component.scss']
})
export class WNumberComponent {

  @Input() column: MetadataColumns;
  @Input() mode = 'input';
  @Output() widgetValueChange: EventEmitter<string> = new EventEmitter<string>();

  private _widgetValue = '';

  @Input()
  set widgetValue(value: string) {
    this._widgetValue = value;
  }

  get widgetValue(): string {
    return this._widgetValue;
  }

  widgetValueSubmitted() {
    if (this.widgetValue) {
      this.widgetValueChange.emit(this.widgetValue);
    }
  }

}
