import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WNumberComponent } from './w-number.component';

describe('WNumberComponent', () => {
  let component: WNumberComponent;
  let fixture: ComponentFixture<WNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
