import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WDateComponent } from './w-date.component';

describe('WDateComponent', () => {
  let component: WDateComponent;
  let fixture: ComponentFixture<WDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
