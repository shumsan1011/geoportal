import { Component, Input, OnInit,Output,  OnDestroy, EventEmitter } from '@angular/core';

@Component({
  selector: 'w-date',
  templateUrl: './w-date.component.html',
  styleUrls: ['./w-date.component.css']
})
export class WDateComponent implements OnInit, OnDestroy {
  @Input() attr=<any>{};
  @Input() widgetValue='';
  @Input() mode="input";
  @Output() widgetValueChange: EventEmitter<any>;

  val_field: string = '';


  ngOnInit() {
  }

  getValue(){
    return (this.widgetValue );
  }


  //subscription: Subscription;

  constructor() {
    this.widgetValueChange = new EventEmitter<any>();

  }  

  

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    //this.subscription.unsubscribe();
  }

  onChange($event) {
    this.widgetValue = $event;
    this.widgetValueChange.emit([$event, this.widgetValue, this]);
    //console.log('qqqqqqqqqq',this.widgetValue, this);
  }
}
