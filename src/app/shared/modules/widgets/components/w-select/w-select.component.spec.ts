import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WSelectComponent } from './w-select.component';

describe('WSelectComponent', () => {
  let component: WSelectComponent;
  let fixture: ComponentFixture<WSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
