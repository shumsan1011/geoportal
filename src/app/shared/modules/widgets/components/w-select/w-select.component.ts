import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { MetadataColumns } from 'app/shared/interfaces';

@Component({
  selector: 'w-select',
  templateUrl: './w-select.component.html',
  styleUrls: ['./w-select.component.scss']
})
export class WSelectComponent implements OnInit {
  @Input() attr: MetadataColumns;
  @Input() mode = 'input';
  valueList: Array<any> = [];
  userValue: string = '';
  @Output()
  widgetValueChange: EventEmitter<string> = new EventEmitter<string>();

  private _widgetValue = '';

  ngOnInit() {
    if (
      this.attr.widget &&
      this.attr.widget.properties &&
      'options' in this.attr.widget.properties
    ) {
      const vals: Array<string> = this.attr.widget.properties['options'].split(
        ';'
      );
      for (var i = 0; i < vals.length; i++) {
        let v = vals[i].split(':');
        this.valueList.push({ id: v[0], name: v[1] });
        if (v[0] == this._widgetValue) {
          this.userValue = v[1];
        }
      }
    }
  }

  @Input()
  set widgetValue(value: string) {
    this._widgetValue = value;
  }

  get widgetValue(): string {
    return this._widgetValue;
  }

  getValue() {
    return this._widgetValue;
  }

  constructor() {}

  widgetValueSubmitted($event) {
    this._widgetValue = $event.target.value;
    this.widgetValueChange.emit(this._widgetValue);
  }
}
