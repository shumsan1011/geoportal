import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'f-date',
  templateUrl: './f-date.component.html',
  styleUrls: ['./f-date.component.css']
})
export class FDateComponent implements OnInit {

  @Input() attr = <any>{};
  @Input() widgetValue: Array<any> = [];
  @Input() mode = {};
  @Output() valueChange: EventEmitter<any>;
  curentval1: string = '';
  curentval2: string = '';


  ngOnInit() {
  }
  constructor() {
    this.valueChange = new EventEmitter<any>();
  }

  changeval1(mas) {
    this.curentval1 = mas[1];
  }

  changeval2(mas) {
    this.curentval2 = mas[1];
  }

  addcondition(){
    console.log(this.widgetValue);
    if(!this.widgetValue){
      this.widgetValue=[];
    }
    let item2 = {v1:this.curentval1 , v2:this.curentval2};
    this.widgetValue.push(item2);
    //mas[2].widgetValue = '';
  }

  deleteitem(val){
    if(!this.widgetValue){
      return;
    }
    var index = this.widgetValue.indexOf(val, 0);
    if (index > -1) {
      this.widgetValue.splice(index, 1);
    }
  }

}
