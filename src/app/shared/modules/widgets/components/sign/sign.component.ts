import { Component, Input } from '@angular/core';
import { SignOptions } from 'app/shared/interfaces';

@Component({
  selector: 'gp-sign',
  templateUrl: './sign.component.html',
  styleUrls: ['./sign.component.scss']
})
export class SignComponent {
  @Input() sign: SignOptions = null;
  @Input() position: 'left' | 'right' = 'left';

  inverseSign(sign: SignOptions): SignOptions {
    switch (sign) {
      case '>':
        return SignOptions.LessThan;
      case '<':
        return SignOptions.GreaterThan;
      case '>=':
        return SignOptions.LessThanOrEqualTo;
      case '<=':
        return SignOptions.GreaterThanOrEqualTo;
      default:
        return sign;
    }

  }

  get symbol() {
    let sign = this.sign;
    if (this.position === 'right') {
      sign = this.inverseSign(sign);
    }
    switch (sign) {
      case SignOptions.Equals:
        return '=';
      case SignOptions.NotEqualTo:
        return '≠';
      case SignOptions.GreaterThan:
        return '>';
      case SignOptions.LessThan:
        return '<';
      case SignOptions.GreaterThanOrEqualTo:
        return '≥';
      case SignOptions.LessThanOrEqualTo:
        return '≤';
      default:
        throw Error('Unknown Sign Options');
    }
  }
}
