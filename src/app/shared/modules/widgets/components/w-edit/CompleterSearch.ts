import { CompleterService, CompleterData, CompleterItem } from 'ng2-completer';
import { APIService } from 'app/shared/services/api.service';
import { Subject } from 'rxjs/Subject';
import { MetadataFilter, DatasetData } from 'app/shared/interfaces';

export class CompleterSearch extends Subject<CompleterItem[]>
    implements CompleterData {
    constructor(
        private datatableService: APIService,
        private dataset_id: number,
        private fieldname: string
    ) {
        super();
    }
    public search(term: string): void {
        let filter: MetadataFilter = {
            fieldname: 'name',
            valueList: [term]
        };
        this.datatableService
            .listObservable(this.dataset_id, 5, 0, [filter])
            //this.http.get("http://mysafeinfo.com/api/data?list=seinfeldepisodes&format=json&nm=" + term + ",contains")
            .map((res: DatasetData) => {
                // Convert the result to CompleterItem[]
                let data = res;
                let matches: CompleterItem[] = data.aaData.map((row: any) =>
                    this.convertToItem(row)
                );
                this.next(matches);
            })
            .subscribe();
    }

    public cancel() {
        // Handle cancel
    }

    public convertToItem(data: any): CompleterItem | null {
        if (!data) {
            return null;
        }
        // data will be string if an initial value is set
        return {
            title: data[this.fieldname],
            originalObject: data.id
        } as CompleterItem;
    }
}
