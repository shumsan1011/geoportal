import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WEditComponent } from './w-edit.component';

describe('WEditComponent', () => {
  let component: WEditComponent;
  let fixture: ComponentFixture<WEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
