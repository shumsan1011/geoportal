import {
  Component,
  Input,
  Output,
  EventEmitter,
  HostListener,
  OnDestroy,
  OnInit
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  MetadataColumns,
  MetadataFilter,
  Metadata
} from 'app/shared/interfaces';
import { APIService } from 'app/shared/services/api.service';
import { CompleterSearch } from './CompleterSearch';

@Component({
  selector: 'w-edit',
  templateUrl: 'w-edit.component.html',
  styleUrls: ['w-edit.component.css'],
  providers: []
})
export class WEditComponent implements OnInit, OnDestroy {
  @Input()
  column: MetadataColumns = {
    fieldname: '',
    tablename: '',
    sqltablename: '',
    sqlname: '',
    widget: { name: '', properties: {} }
  };
  @Input() mode = 'input';
  @Output() widgetValueChange: EventEmitter<string> = new EventEmitter<string>();

  public customData: CompleterSearch;
  refcolumns: Array<MetadataColumns> = [];

  private _widgetValue = '';

  ngOnInit() {
    if (!this.column) {
      this.column = {
        fieldname: '',
        tablename: '',
        sqltablename: '',
        sqlname: '',
        widget: { name: '', properties: {} }
      };
    }

    if (this.mode === 'properites') {
      this.changeautocompletedataset(this.column.widget.properties.dataset_id);
    } else if (this.mode === 'forminput') {
      if (
        this.column.widget.properties.autocomplete &&
        this.column.widget.properties.dataset_id
      ) {
        this.customData = new CompleterSearch(
          this.datatableService,
          this.column.widget.properties.dataset_id,
          this.column.widget.properties.autocompletefield
        );
      } else {
        this.customData = new CompleterSearch(
          this.datatableService,
          100,
          'name'
        );
      }
    }
  }

  ngOnDestroy() {}

  @Input()
  set widgetValue(value: string) {
    this._widgetValue = value;
  }

  get widgetValue(): string {
    return this._widgetValue;
  }

  getValue() {
    return this._widgetValue;
  }

  constructor(private datatableService: APIService) {}

  widgetValueSubmitted($event) {
    if ($event) {
      this.widgetValue = $event.target.value;
    }
    this.widgetValueChange.emit(this.widgetValue);
  }
  keyup($event) {
    if ($event.keyCode === 13) {
      this.widgetValueChange.emit(this.widgetValue);
    }
  }
  changeautocompletedataset(dataset_id) {
    this.column.widget.properties.dataset_id = dataset_id;
    this.refcolumns = [];

    this.datatableService.meta(dataset_id).then((serviceMeta: Metadata) => {
      this.refcolumns = serviceMeta.columns;
    });
  }
}