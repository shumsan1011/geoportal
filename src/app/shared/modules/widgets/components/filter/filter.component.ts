import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MetadataColumns, MetadataFilter } from 'app/shared/interfaces';

@Component({
  moduleId: module.id,
  selector: 'w-filter',
  templateUrl: 'filter.component.html',
  styleUrls: ['filter.component.css']
})
export class WFilterComponent {
  @Input() column: MetadataColumns;
  @Input() mode = 'forminput';
  @Input() filterValue: MetadataFilter = {} as any;
  @Output() filterChange: EventEmitter<MetadataFilter> = new EventEmitter<MetadataFilter>();

  changeVal(newValue: Array<string>) {
    console.log('w-filter newValue = ', newValue);

    // надо получить название столбца, например Authors
    // this.filterObject.fieldName = this.attr.fieldname;
    // this.filterObject.filterValues = newValue;
    // this.filterChange.emit([this.filterObject]);
  }

  passFilter(filter: MetadataFilter) {
    this.filterChange.emit(filter);
  }
}
