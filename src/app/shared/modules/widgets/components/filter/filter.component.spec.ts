import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WFilterComponent } from './w-filter.component';

describe('WFilterComponent', () => {
  let component: WFilterComponent;
  let fixture: ComponentFixture<WFilterComponent>;

  beforeEach(async( () => {
      TestBed.configureTestingModule({
        declarations: [ WFilterComponent ]
      })
      .compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(WFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
