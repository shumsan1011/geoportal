import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Directive
} from '@angular/core';
import { NumberFilter, SignOptions, ModalResult } from 'app/shared/interfaces';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import {
  AbstractControl,
  ValidationErrors,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';

export interface ModalAnswer {
  result: ModalResult;
  filter: NumberFilter | null;
}

function leftLessThanRight(ac: AbstractControl): ValidationErrors | null {
  if (!ac.get('valueLeft') || !ac.get('valueRight')) {
    return null;
  }
  if (ac.get('valueLeft').value && ac.get('valueRight').value) {
    return ac.get('valueLeft').value > ac.get('valueRight').value
      ? { leftIsGreater: true }
      : null;
  }
}

function requiredMinOneValue(ac: AbstractControl): ValidationErrors | null {
  if (ac.get('valueLeft').value || ac.get('valueRight').value) {
    return null;
  } else {
    return { noValues: true };
  }
}

@Component({
  selector: 'app-adding-dialog',
  templateUrl: './adding-dialog.component.html',
  styleUrls: ['./adding-dialog.component.scss']
})
export class AddingDialogComponent {
  signOptions = SignOptions;
  addingNumberFilter: FormGroup;

  constructor(public fb: FormBuilder, public activeModal: NgbActiveModal) {
    this.addingNumberFilter = this.fb.group(
      {
        signOptionLeft: SignOptions.GreaterThanOrEqualTo,
        valueLeft: null,
        signOptionRight: SignOptions.LessThanOrEqualTo,
        valueRight: null
      },
      { validator: [leftLessThanRight, requiredMinOneValue] }
    );
  }

  get modalFilter(): NumberFilter {
    return <NumberFilter>this.addingNumberFilter.value;
  }

  okAnswer(): void {
    const modalAnswer: ModalAnswer = {
      result: ModalResult.mrOk,
      filter: this.modalFilter
    };
    this.activeModal.close(modalAnswer);
  }

  cancelAnswer(): void {
    const modalAnswer: ModalAnswer = {
      result: ModalResult.mrCancel,
      filter: null
    };
    this.activeModal.close(modalAnswer);
  }

  isSignEqualOnForm(): boolean {
    if ((this.addingNumberFilter.value.signOptionRight === SignOptions.Equals) ||
        (this.addingNumberFilter.value.signOptionRight === SignOptions.NotEqualTo)) {
      return true;
    } else {
      return false;
    }
  }
}



