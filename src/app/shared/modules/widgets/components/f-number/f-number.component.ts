import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  MetadataColumns,
  MetadataFilter,
  ModalResult,
  NumberFilter,
  SignOptions
} from 'app/shared/interfaces';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddingDialogComponent, ModalAnswer } from './adding-dialog/adding-dialog.component';
import { SignComponent } from '../sign/sign.component';

@Component({
  selector: 'f-number',
  templateUrl: './f-number.component.html',
  styleUrls: ['./f-number.component.scss']
})
export class FNumberComponent {
  @Input() column: MetadataColumns;
  @Input() mode = {};
  @Input() widgetValue: MetadataFilter = { fieldname: '', valueList: [] };
  @Output() filterValueChange = new EventEmitter<MetadataFilter>(); // объект генерит события

  private signOptions = SignOptions;

  constructor(
    private modalService: NgbModal
  ) { }

  notifyParent(): void {
    this.widgetValue.fieldname = this.column.fieldname;
    this.widgetValue.valueList = this.column.filterValueList;
    this.filterValueChange.emit(this.widgetValue);
  }

  addItem(item: string): void {
    const modalRef = this.modalService.open(AddingDialogComponent, {size: 'lg'});
    modalRef.result.then(
      (modalAnswer: ModalAnswer) => {
        if (modalAnswer.result !== ModalResult.mrOk) {
          return null;
        }
        console.log(modalAnswer.filter);
        if (this.column.filterValueList.indexOf(modalAnswer.filter) === -1) {
          this.column.filterValueList.push(modalAnswer.filter);
        }
      },
      (dismissResult) => {
        console.log(`dismissResult = ${dismissResult}`);
      }
    );
    // this.notifyParent();
  }

  removeItem(item: NumberFilter): void {
    const index = this.column.filterValueList.indexOf(item, 0);
    if (index > -1) {
      this.column.filterValueList.splice(index, 1);
    }
    console.log(`Item ${item} was removed from f-number component`);
    this.notifyParent();
  }

  onlyLeft(filter: NumberFilter): boolean {
    return filter.valueLeft && !filter.valueRight ? true : false;
  }

  onlyRight(filter: NumberFilter): boolean {
    return !filter.valueLeft && filter.valueRight ? true : false;
  }

  both(filter: NumberFilter): boolean {
    return filter.valueLeft && filter.valueRight ? true : false;
  }

}
