import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FNumberComponent } from './f-number.component';

describe('FNumberComponent', () => {
  let component: FNumberComponent;
  let fixture: ComponentFixture<FNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
