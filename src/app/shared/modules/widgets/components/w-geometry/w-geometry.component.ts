import { Component, Input, ElementRef, Output, OnInit, OnDestroy, EventEmitter, HostListener } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { DocService, fieldval } from 'app/shared/services/doc.service';
import { MetadataColumns } from 'app/shared/interfaces';
import { AppActions } from 'app/store/app.actions';
import { Store } from '@ngrx/store';
import { stateGetter } from 'app/store/app.reducer';
import 'rxjs/add/operator/take';

@Component({
  moduleId: module.id,
  templateUrl: 'w-geometry.component.html',
  providers: [DocService]
})
export class WGeometryComponent implements OnInit {
  @Input() attr: MetadataColumns;
  @Output() widgetValueChange: EventEmitter<string> = new EventEmitter<string>();
  @Input() mapId: String = '';

  refcolumns: Array<MetadataColumns> = [];

  _ownValue = '';
  _mode = '';

  // Specifies if the geomtery editing is enabled. The mode can be 'forminput',
  // but triggering the editing for geometry fields requires extra actions (double click)
  editingIsEnabled: Boolean = false;

  subscription: any;

  _widgetValue = '';

  constructor(public docService: DocService, public store: Store<any>, public _el: ElementRef, public actions: AppActions) {
    this._ownValue = this.widgetValue;
  }
  
  @Input()
  set mode(value: string) {
    this._mode = value;
  }

  @Input()
  set widgetValue(value: string) {
    this._widgetValue = value;
  }

  get widgetValue(): string {
    return this._widgetValue;
  }

  getValue() {
    return this._widgetValue;
  }

  onWidgetValueChanged() {
    if (this._ownValue) {
      this.widgetValueChange.emit(this._ownValue);
      this._ownValue = '';
    }
  }

  ngOnInit() {
    this._ownValue = this.widgetValue;   
    if (this.mapId === '') {
      throw 'The map identifier has to be specified for the point widget';
    }
  }

  stopDrawing() {
    console.log('stopDrawing');
    this.actions.cancelDrawing({ mapId: this.mapId });
    if (this.subscription) this.subscription.unsubscribe();
    this.editingIsEnabled = false;
  }

  /**
   * Processes click in any arbitrary area of the page in order to disable the input.
   * Note: it does not happen when we finish the line or polygon drawing.
   */
  onClick(event) {
    if (this.editingIsEnabled && this._mode === 'forminput' && !this._el.nativeElement.contains(event.target)) {
      this.store.take(1).subscribe(globalState => {
        let state = globalState.state;
        let mapElementIds = [];
        for (let key in state) {
          if (key.substring(0, 5) === 'maps_') {
            mapElementIds.push(key);
          }
        }

        let eventHappenedInOneOFMaps = false;
        mapElementIds.map(elementId => {
          let map = document.getElementById(elementId);
          if (map.contains(event.target)) {
            eventHappenedInOneOFMaps = true;
            return false;
          }
        });

        if (eventHappenedInOneOFMaps === false) {
          this.stopDrawing();
        }
      });
    }
  }

  @HostListener('keydown', ['$event']) onKeyDown(event) {
    const e = <KeyboardEvent>event;
    if (e.keyCode === 13) {
      this.onWidgetValueChanged();
      return;
    }
  }
}