import { Component, OnInit, Input, Output, OnDestroy, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'w-password',
  templateUrl: './w-password.component.html',
  styleUrls: ['./w-password.component.css']
})

export class WPasswordComponent {
  @Input() attr = {};
  @Input() mode = "input";

  inputType = 'password';
  passwordTry1 = '';
  passwordTry2 = '';
  passwordsDoNotMatch = false;
  passwordHasWrongFormat = false;

  // @Input() onChange;
  @Input() widgetValue = '';
  @Output() widgetValueChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() {}

  getvalue() {
    return (this.widgetValue);
  }

  onBlur(event) {
    this.passwordsDoNotMatch = false;
    this.passwordHasWrongFormat = false;
    if (this.passwordTry1.length > 0 || this.passwordTry2.length > 0) {
      if (this.passwordTry1 === this.passwordTry2) {
        if (this.scorePassword(this.passwordTry1) > 60) {
          this.widgetValue = this.passwordTry1;
          this.widgetValueChange.emit(this.widgetValue);
        } else {
          this.passwordHasWrongFormat = true;
        }
      } else {
        this.passwordsDoNotMatch = true;
      }
    }
  }

  changeInputType(event) {
    if (this.inputType === 'password') {
      this.inputType = 'text';
    } else {
      this.inputType = 'password';
    }
  }

  /**
   * Scoring password (by https://stackoverflow.com/questions/948172/password-strength-meter)
   * 
   * @param pass Password to check
   */
  scorePassword (pass) {
    let score = 0;
    if (!pass) return score;

    let letters = new Object();
    for (var i=0; i<pass.length; i++) {
      letters[pass[i]] = (letters[pass[i]] || 0) + 1;
      score += 5.0 / letters[pass[i]];
    }

    let variations = {
      digits: /\d/.test(pass),
      lower: /[a-z]/.test(pass),
      upper: /[A-Z]/.test(pass),
      nonWords: /\W/.test(pass),
    }

    let variationCount = 0;
    for (var check in variations) {
      variationCount += (variations[check] == true) ? 1 : 0;
    }

    score += (variationCount - 1) * 10;

    return score;
  }
  
}
