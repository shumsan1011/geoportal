import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { createAction } from './createAction';
import { AppState, TableContext } from './app.state';
import {
    MetadataFilter,
    Metadata,
    SortingField,
    MetadataSorting
} from 'app/shared/interfaces';

interface AuthenticationReply {
    accessToken: string;
    refreshToken: string;
}

@Injectable()
export class AppActions {
    static SET_FRONT_PAGE_MAP_REF = 'SET_FRONT_PAGE_MAP_REF';
    static SET_FRONT_PAGE_MAP_HOST = 'SET_FRONT_PAGE_MAP_HOST';
    static SET_USER = 'SET_USER';
    static UNSET_USER = 'UNSET_USER';
    static SHOW_TABLE = 'SHOW_TABLE';
    static ADD_MAP = 'ADD_MAP';
    static ENABLE_DRAWING_FOR_MAP = 'ENABLE_DRAWING_FOR_MAP';
    static CANCEL_DRAWING = 'CANCEL_DRAWING';
    static SET_LAST_POINT_FOR_MAP = 'SET_LAST_POINT_FOR_MAP';
    static SET_LAST_LINE_FOR_MAP = 'SET_LAST_LINE_FOR_MAP';
    static SET_LAST_POLYGON_FOR_MAP = 'SET_LAST_POLYGON_FOR_MAP';
    static TABLE_DATA_UPDATED = '[Tables] Table Data Updated';
    static SET_FILTER_FOR_TABLE = '[Tables] Set Filter For Table';
    static CLEAR_TABLE_FILTERS = '[Tables] Clear Table Filters'; // Not implemented yet
    static SET_METADATA_FOR_TABLE = '[Tables] Set Metadata For Table';
    static SET_PAGE_FOR_TABLE = '[Tables] Set Page For Table';
    static SET_PAGE_SIZE_FOR_TABLE = '[Tables] Set Page Size For Table';
    static SET_COLLECTION_SIZE_FOR_TABLE = '[Tables]  Set Collection Size For Table';
    static SET_TABLE_FILTERING_BAR_VISIBILITY = '[Tables] Set Visibilty of Table Filtering Bar';
    static SET_TABLE_AGGREGATION_BAR_VISIBILITY = '[Tables] Set Visibilty of Table Aggregation Bar';
    static SET_SORTING_DIRECTION_FOR_TABLE_COLUMN = '[Tables] Set Sorting Direction For Table Column';

    constructor(private store: Store<AppState>) {}

    setFrontPageMapHost(data) {
        this.store.dispatch(
            createAction(AppActions.SET_FRONT_PAGE_MAP_HOST, data)
        );
    }

    setFrontPageMapRef(data) {
        this.store.dispatch(
            createAction(AppActions.SET_FRONT_PAGE_MAP_REF, data)
        );
    }

    enableDrawingForMap(data) {
        this.store.dispatch(
            createAction(AppActions.ENABLE_DRAWING_FOR_MAP, data)
        );
    }

    cancelDrawing(data) {
        this.store.dispatch(createAction(AppActions.CANCEL_DRAWING, data));
    }

    setLastPointForMap(data) {
        this.store.dispatch(
            createAction(AppActions.SET_LAST_POINT_FOR_MAP, data)
        );
    }

    setLastLineForMap(data) {
        this.store.dispatch(
            createAction(AppActions.SET_LAST_LINE_FOR_MAP, data)
        );
    }

    setLastPolygonForMap(data) {
        this.store.dispatch(
            createAction(AppActions.SET_LAST_POLYGON_FOR_MAP, data)
        );
    }

    addMap(map) {
        this.store.dispatch(createAction(AppActions.ADD_MAP, map));
    }

    setUser(tokens: AuthenticationReply) {
        this.store.dispatch(createAction(AppActions.SET_USER, tokens));
    }

    unsetUser() {
        this.store.dispatch(createAction(AppActions.UNSET_USER));
    }

    showTable(datasetId: any, context: TableContext) {
        this.store.dispatch(
            createAction(AppActions.SHOW_TABLE, {
                datasetId: datasetId,
                context: context
            })
        );
    }

    setTableFilter(datasetId: number, filter: MetadataFilter) {
        this.store.dispatch(
            createAction(AppActions.SET_FILTER_FOR_TABLE, {
                datasetId: datasetId,
                filter: filter
            })
        );
    }

    setSortingForTableColumn(datasetId: number, sorting: MetadataSorting) {
        this.store.dispatch(
            createAction(AppActions.SET_SORTING_DIRECTION_FOR_TABLE_COLUMN, {
                datasetId: datasetId,
                sorting: sorting
            })
        );
    }

    setMetadataForTable(datasetId: number, metadata: Metadata) {
        this.store.dispatch(
            createAction(AppActions.SET_METADATA_FOR_TABLE, {
                datasetId: datasetId,
                metadata: metadata
            })
        );
    }

    setPaginatorPageForTable(datasetId: number, page: number) {
        this.store.dispatch(
            createAction(AppActions.SET_PAGE_FOR_TABLE, {
                datasetId: datasetId,
                page: page
            })
        );
    }

    setPaginatorPageSizeForTable(datasetId: number, pageSize: number) {
        this.store.dispatch(
            createAction(AppActions.SET_PAGE_SIZE_FOR_TABLE, {
                datasetId: datasetId,
                pageSize: pageSize
            })
        );
    }

    setCollectionSizeForTable(datasetId: number, collectionSize: number) {
        this.store.dispatch(
            createAction(AppActions.SET_COLLECTION_SIZE_FOR_TABLE, {
                datasetId: datasetId,
                collectionSize: collectionSize
            })
        );
    }

    setFilteringBarVisibilty(datasetId: number, visibility: boolean): void {
        this.store.dispatch(
            createAction(AppActions.SET_TABLE_FILTERING_BAR_VISIBILITY, {
                datasetId: datasetId,
                visibility: visibility
            })
        );
    }

    setAggregationBarVisibilty(datasetId: number, visibility: boolean): void {
        this.store.dispatch(
            createAction(AppActions.SET_TABLE_AGGREGATION_BAR_VISIBILITY, {
                datasetId: datasetId,
                visibility: visibility
            })
        );
    }

    updateTableData(datasetId: number): void {
        this.store.dispatch(
            createAction(AppActions.TABLE_DATA_UPDATED, datasetId)
        );
    }
}
