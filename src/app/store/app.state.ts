import {
  MetadataFilter,
  AuthenticationReply,
  SortingField,
  Metadata
} from '../shared/interfaces';

interface UserDescriptionInterface {
  id: number;
  name: string;
  tokens: {
    accessToken: string;
    refreshToken: string;
  };
}

export enum TableContext {
  TableList,
  ServiceTables
}

export interface DataTableState {
  dataset_id: number;
  meta?: Metadata;
  data?: any;
  page?: number;
  pageSize?: number;
  collectionSize?: number;
  // filters?: MetadataFilter[]; // вынесено в column // meta.columns[i].filterValueList
  // sort?: SortingField[]; // вынесено в column
  showFilteringBar?: boolean;
  showAggregationBar?: boolean;
  context: TableContext;
}

export interface AppState {
  readonly user: Boolean | UserDescriptionInterface;
  readonly tables: Object | DataTableState[]; // у нас никогда не массив, а объекты
  // readonly filters: MetadataFilter[];
  readonly maps: Object;
  readonly tableOfTables: DataTableState;

}
