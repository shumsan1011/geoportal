import { Action } from '@ngrx/store';

export interface ActionWithPayload extends Action {
    payload?: any;
}

export function createAction(type, payload?): ActionWithPayload {
    return { type, payload };
}
