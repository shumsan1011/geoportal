import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ActionWithPayload } from 'app/store/createAction';
import { AppActions } from './app.actions';
import { AppState, TableContext } from './app.state';
import { Action } from '@ngrx/store';
import { DataTableState } from 'app/store/app.state';
import { MetadataFilter } from 'app/shared/interfaces';
import { find } from 'lodash';

interface UserDataInterface {
  id: number;
  name: string;
};

const initialState = {
  user: false,
  tables: [],
  filters: [],
  maps: {
    frontPageMap: {
      ref: false,
      host: false
    }
  }
};

export function appReducer(
  state = initialState,
  action: ActionWithPayload
): Object {
  console.warn('REDUCER SAYS: ', action);

  const newMap = {};
  let mapProperties;
  switch (action.type) {
    case AppActions.SET_FRONT_PAGE_MAP_HOST:
      mapProperties = Object.assign({}, state.maps['frontPageMap']);
      mapProperties['ref'] = state.maps.frontPageMap.ref;
      mapProperties['host'] = action.payload.host;
      return {
        ...state,
        maps: {
          ...state.maps,
          frontPageMap: mapProperties
        }
      };
    case AppActions.SET_FRONT_PAGE_MAP_REF:
      mapProperties = Object.assign({}, state.maps['frontPageMap']);
      mapProperties['ref'] = action.payload.ref;
      mapProperties['host'] = state.maps.frontPageMap.host;
      return {
        ...state,
        maps: {
          ...state.maps,
          frontPageMap: mapProperties
        }
      };
    case AppActions.SET_LAST_POINT_FOR_MAP:
      mapProperties = Object.assign({}, state.maps[action.payload.mapId]);
      mapProperties['drawingIsEnabled'] = false;
      mapProperties['lastPoint'] = action.payload.wkt;
      return {
        ...state,
        maps: {
          ...state.maps,
          [action.payload.mapId]: mapProperties
        }
      };
    case AppActions.SET_LAST_LINE_FOR_MAP:
      mapProperties = Object.assign({}, state.maps[action.payload.mapId]);
      mapProperties['drawingIsEnabled'] = false;
      mapProperties['lastLine'] = action.payload.wkt;
      return {
        ...state,
        maps: {
          ...state.maps,
          [action.payload.mapId]: mapProperties
        }
      };
    case AppActions.SET_LAST_POLYGON_FOR_MAP:
      mapProperties = Object.assign({}, state.maps[action.payload.mapId]);
      mapProperties['drawingIsEnabled'] = false;
      mapProperties['lastPolygon'] = action.payload.wkt;
      return {
        ...state,
        maps: {
          ...state.maps,
          [action.payload.mapId]: mapProperties
        }
      };
    case AppActions.CANCEL_DRAWING:
      mapProperties = Object.assign({}, state.maps[action.payload.mapId]);
      mapProperties['drawingIsEnabled'] = false;
      return {
        ...state,
        maps: {
          ...state.maps,
          [action.payload.mapId]: mapProperties
        }
      };

    case AppActions.ENABLE_DRAWING_FOR_MAP:
      mapProperties = Object.assign({}, state.maps[action.payload.mapId]);
      mapProperties['drawingIsEnabled'] = true;
      mapProperties['drawingMode'] = action.payload.mode;
      mapProperties['existingFeature'] = action.payload.existingFeature ? action.payload.existingFeature : false;
      mapProperties['lastPoint'] = false;
      return {
        ...state,
        maps: {
          ...state.maps,
          [action.payload.mapId]: mapProperties
        }
      };

    case AppActions.ADD_MAP:
      if (state[action.payload.mapId]) {
        throw Error(`Map with specified id already exists`);
      }

      return {
        ...state,
        maps: {
          ...state.maps,
          [action.payload.mapId]: { drawingIsEnabled: false }
        }
      };

    case AppActions.SET_USER:
      const splittedToken = action.payload.accessToken.split('.');
      const user = JSON.parse(atob(splittedToken[1]));
      return (<any>Object).assign({}, state, {
        user: {
          id: user.id,
          name: user.username,
          tokens: action.payload
        }
      });

    case AppActions.UNSET_USER:
      return (<any>Object).assign({}, state, { user: false });

      case AppActions.SHOW_TABLE:
      return {
        ...state,
        tables: {
          ...state.tables,
          [action.payload.datasetId]: {
            dataset_id: action.payload.datasetId,
            sort: [],
            lastUpdated: Date.now(),
            context: TableContext.TableList
          }
        }
      };

    // следует добавить action ДОБАВИТЬ_ТАБЛИЦУ_К_СПИСКУ_ТАБЛИЦ

    case AppActions.SET_METADATA_FOR_TABLE:
      const newContext: TableContext = state.tables[action.payload.datasetId]
        ? state.tables[action.payload.datasetId].context
        : TableContext.ServiceTables;
      return {
        ...state,
        tables: {
          ...state.tables,
          [action.payload.datasetId]: {
            ...state.tables[action.payload.datasetId],
            dataset_id: action.payload.datasetId, // для случаю открытия по data set id
            meta: action.payload.metadata,
            lastUpdated: Date.now(),
            context: newContext
          }
        }
      };

    case AppActions.SET_PAGE_FOR_TABLE:
      return {
        ...state,
        tables: {
          ...state.tables,
          [action.payload.datasetId]: {
            ...state.tables[action.payload.datasetId],
            page: action.payload.page,
            lastUpdated: Date.now()
          }
        }
      };

    case AppActions.SET_PAGE_SIZE_FOR_TABLE:
      return {
        ...state,
        tables: {
          ...state.tables,
          [action.payload.datasetId]: {
            ...state.tables[action.payload.datasetId],
            pageSize: action.payload.pageSize,
            lastUpdated: Date.now()
          }
        }
      };

    case AppActions.SET_COLLECTION_SIZE_FOR_TABLE:
      return {
        ...state,
        tables: {
          ...state.tables,
          [action.payload.datasetId]: {
            ...state.tables[action.payload.datasetId],
            collectionSize: action.payload.collectionSize,
            lastUpdated: Date.now()
          }
        }
      };

    case AppActions.SET_TABLE_FILTERING_BAR_VISIBILITY:
      return {
        ...state,
        tables: {
          ...state.tables,
          [action.payload.datasetId]: {
            ...state.tables[action.payload.datasetId],
            showFilteringBar: action.payload.visibility,
            lastUpdated: Date.now()
          }
        }
      };

    case AppActions.SET_TABLE_AGGREGATION_BAR_VISIBILITY:
      return {
        ...state,
        tables: {
          ...state.tables,
          [action.payload.datasetId]: {
            ...state.tables[action.payload.datasetId],
            showAggregationBar: action.payload.visibility,
            lastUpdated: Date.now()
          }
        }
      };

    case AppActions.SET_FILTER_FOR_TABLE: {
      const choosedColumn = { ...find(state.tables[action.payload.datasetId].meta.columns,
        { 'fieldname': action.payload.filter.fieldname }
      ) };
      choosedColumn['filterValueList'] = action.payload.filter.valueList;

      const newColumns = state.tables[action.payload.datasetId].meta.columns.map(column => {
        if (column.fieldname === action.payload.filter.fieldname) {
          return choosedColumn;
        }
        return column;
      });

      return {
        ...state,
        tables: {
          ...state.tables,
          [action.payload.datasetId]: {
            ...state.tables[action.payload.datasetId],
            meta: {
              ...state.tables[action.payload.datasetId].meta,
              columns: newColumns
            },
            lastUpdated: Date.now(),
          }
        }
      };
    }

    case AppActions.SET_SORTING_DIRECTION_FOR_TABLE_COLUMN: {
      const choosedColumn = { ...find(state.tables[action.payload.datasetId].meta.columns,
        { 'fieldname': action.payload.sorting.fieldname }
      ) };
      choosedColumn['sortingOrder'] = action.payload.sorting.sorting;

      const newColumns = state.tables[action.payload.datasetId].meta.columns.map(column => {
        if (column.fieldname === action.payload.sorting.fieldname) {
          return choosedColumn;
        }
        return column;
      });

      return {
        ...state,
        tables: {
          ...state.tables,
          [action.payload.datasetId]: {
            ...state.tables[action.payload.datasetId],
            meta: {
              ...state.tables[action.payload.datasetId].meta,
              columns: newColumns
            },
            lastUpdated: Date.now(),
          }
        }
      };
    }

    case AppActions.TABLE_DATA_UPDATED:
      return {
        ...state,
        tables: {
          ...state.tables,
          [action.payload]: {
            ...state.tables[action.payload],
            lastUpdated: Date.now(),
          }
        }
      };

    default:
      console.warn(`Unprocessed action ${action.type}`);
      return state;
  }
}

const getUnderlyingProperty = (currentStateLevel, properties: Array<any>) => {
  if (properties.length === 0) {
    throw Error('Unable to get the underlying property');
  } else if (properties.length === 1) {
    const key = properties.shift();
    return currentStateLevel[key];
  } else {
    const key = properties.shift();
    return getUnderlyingProperty(currentStateLevel[key], properties);
  }
};

export const stateGetter = (...args) => {
  return (state: AppState) => {
    const argsCopy = args.slice();
    return getUnderlyingProperty(state['state'], argsCopy);
  };
};
