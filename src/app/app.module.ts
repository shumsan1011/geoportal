import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StoreModule } from '@ngrx/store';
import { CommonModule } from '@angular/common';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgForageModule, NgForageConfig } from 'ngforage';
import { Ng2CompleterModule } from 'ng2-completer';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';
import { NgObjectPipesModule } from 'ngx-pipes';

import { AppComponent } from './app.component';
import { AuthorizationHeaderInterceptor } from 'app/interceptors/authorization-header-interceptor';

import { MapToolsAPIService } from 'app/shared/api/maptools.api.service';

import { AppActions } from './store/app.actions';
import { appReducer } from './store/app.reducer';

import { ServiceManagerModule } from './modules/service-manager/service-manager.module';
import { UsersModule } from './modules/users/users.module';
import { SharedModule } from './shared/shared.module';
import { WidgetsModule } from './shared/modules/widgets/widgets.module';
import { DashboardPageComponent } from 'app/modules/dashboard/pages/dashboard/dashboard.page';
import { DashboardModule } from './modules/dashboard/dashboard.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        StoreModule.forRoot({ state: appReducer }),
        StoreDevtoolsModule.instrument({ maxAge: 50 }),
        BrowserModule,
        HttpClientModule,
        Ng2CompleterModule,
        FormsModule,
        LeafletModule.forRoot(),
        LeafletDrawModule.forRoot(),
        NgObjectPipesModule,
        NgbModule.forRoot(),
        NgForageModule.forRoot(),
        ServiceManagerModule,
        DashboardModule,
        UsersModule,
        SharedModule,
        WidgetsModule,
        RouterModule.forRoot([], { enableTracing: false })
    ],
    declarations: [AppComponent],
    providers: [
        AppActions,
        MapToolsAPIService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthorizationHeaderInterceptor,
            multi: true
        }
    ],
    entryComponents: [DashboardPageComponent],
    bootstrap: [AppComponent]
})
export class AppModule {
    public constructor(ngfConfig: NgForageConfig) {
        ngfConfig.configure({
            name: 'gp-app',
            driver: [
                NgForageConfig.DRIVER_INDEXEDDB,
                NgForageConfig.DRIVER_LOCALSTORAGE
            ]
        });
    }
}
