import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { State, Store } from '@ngrx/store';
import { AppState } from 'app/store/app.state';
import { stateGetter } from 'app/store/app.reducer';

@Injectable()
export class AuthorizationHeaderInterceptor implements HttpInterceptor {
    private _user: any;

    constructor(private store: Store<AppState>) {
        this.store.select(stateGetter('user')).subscribe(data => {
            this._user = data;
        });
    }

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        if (this._user) {
            const clonedRequest = req.clone({
                headers: req.headers.set(
                    'Authorization',
                    `Bearer ${this._user.tokens.accessToken}`
                )
            });

            return next.handle(clonedRequest);
        } else {
            return next.handle(req);
        }
    }
}
