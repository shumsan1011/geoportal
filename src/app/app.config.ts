import { environment } from '../environments/environment';

export interface AppConfig {
  APIPath: string;
  systemTablesIdentifiers: Object;
}

export const AppConfig: AppConfig = {
    APIPath: environment.APIPath,
    // Keep it actualized with the Geoportal REST API configuration
    systemTablesIdentifiers: {
      MAPS: 2,
      SYMBOLS: 3,
      ROLES: 4,
      USERS: 5,
      USER_ACTIVATION_CODES: 6,
      TABLE_ACCESS_RULES: 9,
      DATASET_TREE: 10,
      SAMPLE_THEME: 20,
      GEO_DATASET: 100,
      WPS_METHODS: 185,
      METHOD_EXAMPLES: 186,
      FILELINK: 187
    }
};
