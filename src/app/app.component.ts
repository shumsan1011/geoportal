import { Component, ViewChildren, OnInit, QueryList } from '@angular/core';
import { APIService } from './shared/services/api.service';

@Component({
    selector: 'app-root',
    template: '<router-outlet></router-outlet>  '
})
export class AppComponent implements OnInit {
    constructor(private apiService: APIService) {}

    ngOnInit() {
        this.apiService.checkAuthentication();
    }
}
