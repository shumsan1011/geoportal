import { Component, OnInit } from '@angular/core';
import { APIService } from 'app/shared/services/api.service';
import { AppConfig } from '../../../../app.config';

@Component({
  selector: 'gp-user-registration',
  templateUrl: './registration.component.html'
})
export class RegistrationComponent implements OnInit {
  userTableMeta: any = false;
  registered = false;

  constructor(private apiService: APIService) {
    this.apiService.meta(AppConfig.systemTablesIdentifiers['USERS']).then(result => {
      this.userTableMeta = result;
    });
  }

  onSuccessfulRegistration() {
    this.registered = true;
  }

  ngOnInit() {}
}
