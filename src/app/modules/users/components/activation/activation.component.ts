import { Component, OnInit } from '@angular/core';
import { APIService } from '../../../../shared/services/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'gp-user-activation',
  templateUrl: './activation.component.html',
})
export class ActivationComponent implements OnInit {
  activationStatusReceived = false;
  successfullActivation = false;

  constructor(private activatedRoute: ActivatedRoute, private apiService: APIService) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.apiService.activateUser(params.code).then(result => {
        this.activationStatusReceived = true;
        this.successfullActivation = true;
      }).catch(errorResponse => {
        this.activationStatusReceived = true;
      });
   });
  }
}
