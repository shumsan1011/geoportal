import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../../shared/shared.module';

import { UsersRoutingModule } from './users-routing.module';
import { RegistrationComponent } from './components/registration/registration.component';
import { ActivationComponent } from './components/activation/activation.component';

@NgModule({
    imports: [CommonModule, FormsModule, SharedModule, UsersRoutingModule],
    declarations: [RegistrationComponent, ActivationComponent]
})
export class UsersModule {}
