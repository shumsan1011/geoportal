import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformationComponent } from './components/information/information.component';
import { EditFormComponent } from './components/edit-form/edit-form.component';
import { RunFormComponent } from './components/run-form/run-form.component';

const routes: Routes = [
  { path: 'service-manager', component: InformationComponent },
  { path: 'service-manager/create', component: EditFormComponent },
  { path: 'service-manager/edit/:id', component: EditFormComponent },
  { path: 'service-manager/run/:id', component: RunFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceManagerRoutingModule { }