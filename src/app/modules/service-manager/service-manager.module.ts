import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AceEditorModule } from 'ng2-ace-editor';

import { SharedModule } from 'app/shared/shared.module';
import { WidgetsModule } from 'app/shared/modules/widgets/widgets.module';

import { ServiceManagerRoutingModule } from './service-manager-routing.module';
import { InformationComponent } from './components/information/information.component';
import { EditFormComponent } from './components/edit-form/edit-form.component';
import { CatalogItemParameterComponent } from './components/catalog-item-parameter/catalog-item-parameter.component';
import { RunFormComponent } from './components/run-form/run-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AceEditorModule,
    SharedModule,
    WidgetsModule,
    ServiceManagerRoutingModule
  ],
  declarations: [
    InformationComponent,
    EditFormComponent,
    CatalogItemParameterComponent,
    RunFormComponent
  ]
})
export class ServiceManagerModule {}