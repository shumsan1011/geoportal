import {
    ElementRef,
    ViewChild,
    Component,
    OnInit,
    OnDestroy
} from '@angular/core';
import { APIService } from 'app/shared/services/api.service';
import { Observable } from 'rxjs/Observable';
import {
    NgbModal,
    ModalDismissReasons,
    NgbModalRef
} from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { CatalogItemParameterComponent } from './../catalog-item-parameter/catalog-item-parameter.component';
import xml2js from 'xml2js';

/* Defining data structures */

interface ServiceServer {
    host: String;
    port: Number;
    path: String;
}

interface ServiceMetadata {
    title: String;
    identifier: String;
}

interface ServiceParameter {
    identifier: String;
    title: String;
    description: String;
    widget: String;
}

const SERVICE_WPS = 'service_wps';
const SCENARIO = 'scenario';

const DEFAULT_SERVICES_SERVER_PORT = 80;

@Component({
    selector: 'app-services-and-scenarios-edit-form',
    templateUrl: './edit-form.component.html',
    styleUrls: ['./edit-form.component.scss']
})
export class EditFormComponent {
    @ViewChild('content') content: ElementRef;
    private modalRef: NgbModalRef;

    public id: Number = -1;
    public retrievedId: Number = -1;
    public name: String = '';
    public textualIdentifier: String = '';
    public description: String = '';
    public serviceIdentifier: String = '';
    public mapReduceSpecification: String = '';
    public type: String;
    public servers: Array<ServiceServer> = [];
    public availableServices: Array<ServiceMetadata> = [];
    public inputParameters: Array<ServiceParameter> = [];
    public outputParameters: Array<ServiceParameter> = [];
    public scenarioCode: String = '';
    public scenarioEditorOptions: any = { maxLines: 1000, printMargin: false };
    public types: Array<Object> = [
        {
            id: SERVICE_WPS,
            title: 'WPS service'
        },
        {
            id: SCENARIO,
            title: 'JavaScript scenario'
        }
    ];

    public host: String = '';
    public port: Number = DEFAULT_SERVICES_SERVER_PORT;
    public path: String = '';

    /**
     * The instaniation of the component
     *
     * @param api {APIService} Reruired to talk with the backend
     */
    constructor(
        private api: APIService,
        private modalService: NgbModal,
        private route: ActivatedRoute
    ) {
        this.servers = [
            {
                host: '84.237.16.46',
                port: 8800,
                path: '/cgi-bin/wps/zoo_loader.cgi?'
            }
        ];

        route.params.map(p => p.id).subscribe(
            id => {
                if (this.modalRef) this.modalRef.close();
                if (parseInt(id) > 0) {
                    this.api
                        .getCatalogItem(id)
                        .then(result => {
                            let data = result['data'];

                            this.name = data['name'];
                            this.textualIdentifier = data['name'];
                            this.description = data['description'];
                            this.type =
                                data['type'] === 'wps'
                                    ? 'service_wps'
                                    : 'scenario';
                            this.serviceIdentifier =
                                data['type'] === 'wps' ? data['wpsmethod'] : '';
                            this.mapReduceSpecification =
                                data['map_reduce_specification'];

                            let wpsServers = [];
                            if (data['wpsservers']) {
                                data['wpsservers'].map(item => {
                                    wpsServers.push({
                                        host: item.host,
                                        port: item.port,
                                        path: item.path
                                    });
                                });
                            }
                            this.servers = wpsServers;

                            let scenarioCode = '';
                            if (data['js_body']) {
                                scenarioCode = data['js_body']
                                    .replace(/XXNEWLINEXX/g, '\n')
                                    .replace(/XXQUOTEXX/g, '"');
                            }
                            this.scenarioCode = scenarioCode;

                            let inputParameters = [];
                            data['params'].map(item => {
                                inputParameters.push({
                                    identifier: item.fieldname,
                                    title: item.title,
                                    description: item.description
                                });
                            });
                            this.inputParameters = inputParameters;

                            let outputParameters = [];
                            data['output_params'].map(item => {
                                outputParameters.push({
                                    identifier: item.fieldname,
                                    title: item.title,
                                    description: item.description
                                });
                            });
                            this.outputParameters = outputParameters;

                            this.id = data['id'];
                        })
                        .catch(error => {
                            throw error;
                        });
                }
            },
            e => console.log('onError: %s', e),
            () => console.log('onCompleted')
        );
    }

    /**
     * Checks if input parameters are all filled up
     */
    private parametersAreFilledUp() {
        if (
            this.inputParameters.length > 0 &&
            this.outputParameters.length > 0
        ) {
            let parametersAreFilledUp = true;
            let combinedParametersArray = this.inputParameters.concat(
                this.outputParameters
            );
            combinedParametersArray.map(item => {
                if (!item.title || item.title.length === 0) {
                    parametersAreFilledUp = false;
                    return false;
                }
            });

            return parametersAreFilledUp;
        } else {
            return false;
        }
    }

    /**
     * Clears input and output parameters on type change
     *
     * @param event {Object} Event object
     */
    private onTypeChange(event) {
        this.inputParameters = [];
        this.outputParameters = [];
        this.type = event.target.value;
    }

    /**
     * Closing the modal
     */
    ngOnDestroy() {
        if (this.modalRef) this.modalRef.close();
    }

    /**
     * Fetches and displays parameters for the specific service.
     *
     * @param serviceIdentifier {String} Service identifier
     */
    private fetchServiceParameter(serviceIdentifier) {
        if (this.servers.length > 0) {
            this.serviceIdentifier = serviceIdentifier;

            let path = this.servers[0].path;
            let requestParameters = [
                'request=DescribeProcess',
                'service=WPS',
                `identifier=${serviceIdentifier}`,
                `version=1`
            ];
            path += requestParameters.join('&');

            let protocol = '';
            if (this.servers[0].host.indexOf('://') === -1) {
                protocol = 'http://';
            }

            this.api
                .proxifyRequest(
                    'GET',
                    `${protocol}${this.servers[0].host}:${
                        this.servers[0].port
                    }${path}`
                )
                .then(data => {
                    xml2js.parseString(data, (err, result) => {
                        const fillParameters = (item, container) => {
                            container.push({
                                identifier: item['ows:Identifier'][0],
                                title: item['ows:Title'][0],
                                description: '',
                                widget: ''
                            });
                        };

                        let inputParameters = [];
                        result['wps:ProcessDescriptions'].ProcessDescription[
                            '0'
                        ].DataInputs[0].Input.map(item =>
                            fillParameters(item, inputParameters)
                        );
                        this.inputParameters = inputParameters;

                        let outputParameters = [];
                        result['wps:ProcessDescriptions'].ProcessDescription[
                            '0'
                        ].ProcessOutputs[0].Output.map(item =>
                            fillParameters(item, outputParameters)
                        );
                        this.outputParameters = outputParameters;
                    });
                });
        } else {
            throw new Error('Unable to fetch services');
        }
    }

    /**
     * Form the service or scenario description object according to the entered data
     *
     * @return {Object}
     */
    private gatherData() {
        let serviceDescription = {
            name: this.textualIdentifier,
            description: this.description,
            type: this.type === SERVICE_WPS ? 'wps' : 'js',
            map_reduce_specification: this.mapReduceSpecification,
            params: [],
            output_params: []
        };

        if (this.type === SERVICE_WPS) {
            serviceDescription['servers'] = this.servers;
            serviceDescription['method'] = this.serviceIdentifier;
        } else if (this.type === SCENARIO) {
            serviceDescription['funcbody'] = this.scenarioCode;
        } else {
            throw new Error(`Unable to detect type`);
        }

        this.inputParameters.map(item => {
            serviceDescription.params.push({
                fieldname: item.identifier,
                title: item.title,
                description: item.description,
                visible: true,
                widget: {
                    name: `edit`,
                    properties: {
                        tsvector: ``
                    }
                }
            });
        });

        this.outputParameters.map(item => {
            serviceDescription.output_params.push({
                fieldname: item.identifier,
                title: item.title,
                description: item.description,
                visible: true,
                widget: {
                    name: `edit`,
                    properties: {
                        tsvector: ``
                    }
                }
            });
        });

        return serviceDescription;
    }

    /**
     * Registers catalog item
     *
     * @param event {Object} Occured event
     */
    register(event) {
        let description = this.gatherData();
        this.api
            .sendCatalogItem(description)
            .then(data => {
                this.retrievedId = data['id'];
                this.modalRef = this.modalService.open(this.content, {
                    backdrop: 'static'
                });
            })
            .catch(error => {
                throw error;
            });
    }

    /**
     * Updates catalog item
     *
     * @param event {Object} Occured event
     */
    update(event) {
        let description = this.gatherData();
        description['previd'] = this.id;
        this.api
            .sendCatalogItem(description)
            .then(data => {
                this.modalRef = this.modalService.open(this.content);
            })
            .catch(error => {
                throw error;
            });
    }

    /**
     * Fetches list of services for the first available services server.
     */
    private fetchServices() {
        if (this.servers.length > 0) {
            let path = this.servers[0].path;
            let requestParameters = ['request=GetCapabilities', 'service=WPS'];
            path += requestParameters.join('&');

            let protocol = '';
            if (this.servers[0].host.indexOf('://') === -1) {
                protocol = 'http://';
            }

            this.api
                .proxifyRequest(
                    'GET',
                    `${protocol}${this.servers[0].host}:${
                        this.servers[0].port
                    }${path}`
                )
                .then(data => {
                    xml2js.parseString(data, (err, result) => {
                        let services = [];
                        result['wps:Capabilities']['wps:ProcessOfferings']['0'][
                            'wps:Process'
                        ].map(item => {
                            services.push({
                                title: item['ows:Title'][0],
                                identifier: item['ows:Identifier'][0]
                            });
                        });

                        services.sort((a, b) => {
                            if (a.title < b.title) return -1;
                            if (a.title > b.title) return 1;
                            return 0;
                        });

                        this.availableServices = services;
                    });
                });
        } else {
            throw new Error('Unable to fetch services');
        }
    }

    /**
     * Deletes services server
     *
     * @param event {Object} Event
     * @param index {Number} Index of the deleted services server
     */
    private deleteServicesServer(event, index) {
        this.servers.splice(index, 1);
    }

    /**
     * Updating specified parameter
     *
     * @param data {Object} The parameter description
     * @param type {String} The type specifier
     * @param index {Number} The index of the parameter
     */
    private onParameterChange(data, type, index) {
        if (type === 'input') {
            this.inputParameters[index] = data;
        } else if (type === 'output') {
            this.outputParameters[index] = data;
        } else {
            throw new Error(`Invalid type was provided`);
        }
    }

    /**
     * Deleting specified parameter
     *
     * @param data {Object} The parameter description
     * @param type {String} The type specifier
     * @param index {Number} The index of the parameter
     */
    private onParameterDelete(data, type, index) {
        if (type === 'input') {
            this.inputParameters.splice(index, 1);
        } else if (type === 'output') {
            this.outputParameters.splice(index, 1);
        } else {
            throw new Error(`Invalid type was provided`);
        }
    }

    /**
     * Adds sample parameter to input or output parameters
     *
     * @param event {Object} Event object
     * @param type {String} The parameter type specifier
     */
    private addSampleParameter(event, type: String) {
        const sampleParameter = {
            identifier: '',
            title: '',
            description: '',
            widget: ''
        };

        if (type === 'input') {
            this.inputParameters.push(sampleParameter);
        } else if (type === 'output') {
            this.outputParameters.push(sampleParameter);
        } else {
            throw new Error(`Ivalid type was provided`);
        }
    }

    /**
     * Adds services server
     */
    private addServiceServer() {
        const newServer = {
            host: this.host,
            port: this.port,
            path: this.path
        };

        this.host = '';
        this.port = DEFAULT_SERVICES_SERVER_PORT;
        this.path = '';
        this.servers.push(newServer);
    }

    /**
     * Transliterates word (cyryllic to latin)
     *
     * @param word {String} Transliterated word
     */
    private transliterate(word) {
        const symbolSet = {
            Ё: 'YO',
            Й: 'I',
            Ц: 'TS',
            У: 'U',
            К: 'K',
            Е: 'E',
            Н: 'N',
            Г: 'G',
            Ш: 'SH',
            Щ: 'SCH',
            З: 'Z',
            Х: 'H',
            Ъ: "'",
            ё: 'yo',
            й: 'i',
            ц: 'ts',
            у: 'u',
            к: 'k',
            е: 'e',
            н: 'n',
            г: 'g',
            ш: 'sh',
            щ: 'sch',
            з: 'z',
            х: 'h',
            ъ: "'",
            Ф: 'F',
            Ы: 'I',
            В: 'V',
            А: 'a',
            П: 'P',
            Р: 'R',
            О: 'O',
            Л: 'L',
            Д: 'D',
            Ж: 'ZH',
            Э: 'E',
            ф: 'f',
            ы: 'i',
            в: 'v',
            а: 'a',
            п: 'p',
            р: 'r',
            о: 'o',
            л: 'l',
            д: 'd',
            ж: 'zh',
            э: 'e',
            Я: 'Ya',
            Ч: 'CH',
            С: 'S',
            М: 'M',
            И: 'I',
            Т: 'T',
            Ь: "'",
            Б: 'B',
            Ю: 'YU',
            я: 'ya',
            ч: 'ch',
            с: 's',
            м: 'm',
            и: 'i',
            т: 't',
            ь: "'",
            б: 'b',
            ю: 'yu'
        };

        return word
            .split('')
            .map(char => {
                return symbolSet[char] || char;
            })
            .join('');
    }

    /**
     * Generates and updates internal textual identifier
     *
     * @param value {String} Current human-readable name value
     */
    public generateTextualIdentifier(value: String) {
        let identifier = this.transliterate(value)
            .toLowerCase()
            .replace(/[^\w\s]/g, '')
            .replace(/[\s]/g, '_');
        this.textualIdentifier = identifier;

        if (this.scenarioCode.split('\n').length <= 3) {
            this.scenarioCode = `function ${identifier}(input, mapping) {
  var abc = 123;
}`;
        }
    }
}
