import { Component } from '@angular/core';
import { APIService } from 'app/shared/services/api.service';
import { ActivatedRoute } from '@angular/router';

const consoleOutputTimeout = 3000;
type CatalogItemRunStatus = 'RUN_STATUS_NULL' | 'RUN_STATUS_SUCCEEDED' | 'RUN_STATUS_CREATED' | 'RUN_STATUS_FAILED';

interface ConsoleLine {
  datetime: Date;
  content: String;
}

@Component({
  selector: 'app-services-and-scenarios-run-form',
  templateUrl: './run-form.component.html'
})

export class RunFormComponent {
  private id: Number = -1;

  private catalogItemRunId: Number = -1;
  private catalogItemRunSTDOUT: Array<ConsoleLine> = [];
  private catalogItemRunSTDERR: Array<ConsoleLine> = [];
  private catalogItemRunResult: any = false;
  private catalogItemStatus: CatalogItemRunStatus = 'RUN_STATUS_NULL';

  private intervalHandle: any = false;
  public catalogItem: Object;
  private inputValues: Array<String> = [];
  private outputValues: Array<String> = [];
  private running: Boolean = false;

  constructor(private api: APIService, private route: ActivatedRoute) {
    route.params.map(p => p.id).subscribe(id => {
      if (parseInt(id) > 0) {
        this.api.getCatalogItem(id).then(result => {
          let serviceData = result['data'];
         
          for (let i = 0; i < serviceData.params.length; i++) {
            this.inputValues[i] = '';
          }

          for (let i = 0; i < serviceData.output_params.length; i++) {
            this.outputValues[i] = '';
          }

          this.catalogItem = {
            id: serviceData.id,
            name: serviceData.name,
            description: serviceData.description,
            inputParameters: serviceData.params,
            outputParameters: serviceData.output_params
          };

          this.id = id;
        });
      }
    });
  }


  /**
   * Checks if input parameters were entered
   * 
   * @return {Boolean}
   */
  private inputParametersAreReady() {
    let inputParametersAreFilledUp = true;
    this.inputValues.map(item => {
      if (item === '') {
        inputParametersAreFilledUp = false;
        return false;
      }
    });

    return inputParametersAreFilledUp;
  }


  /**
   * Detects and returns new console lines that came from server
   * 
   * @param existingLines {Array} Existing console lines
   * @param newLines {String} 
   */
  private retrieveNewConsoleLines(existingLines: Array<any>, newLines: String) {
    let newConsoleLines = newLines.split('\n').map(item => {
      return {
        datetime: new Date(),
        content: item
      };
    });

    newConsoleLines.splice(0, existingLines.length); 
    return newConsoleLines;
  }


  /**
   * Populates the STDOUT console output object
   * 
   * @param data {String} Raw console output
   */
  private populateSTDOUT(data: String) {
    let newConsoleLines = this.retrieveNewConsoleLines(this.catalogItemRunSTDOUT, data);
    this.catalogItemRunSTDOUT = this.catalogItemRunSTDOUT.concat(newConsoleLines);
  }


  /**
   * Populates the STDERR console output object
   * 
   * @param data {String} Raw console output
   */
  private populateSTDERR(data: String) {
    let newConsoleLines = this.retrieveNewConsoleLines(this.catalogItemRunSTDERR, data);
    this.catalogItemRunSTDERR = this.catalogItemRunSTDERR.concat(newConsoleLines);
  }


  /**
   * Starts the interval polling of the catalog item run's console output
   */
  private startIntervalRequest() {
    this.intervalHandle = setInterval(() => {
      this.api.getCatalogItemRunConsoleOutput(this.catalogItemRunId).then(result => {
        console.log(result['data'].lines[0].status);
        switch (result['data'].lines[0].status) {
          case 'METHOD_EXAMPLE_STARTED':
          case 'METHOD_EXAMPLE_CREATED':
            this.catalogItemStatus = 'RUN_STATUS_CREATED';
            this.populateSTDOUT(result['data'].lines[0].console_output);
            break;
          case 'METHOD_EXAMPLE_SUCCEEDED':
            this.catalogItemStatus = 'RUN_STATUS_SUCCEEDED';
            this.catalogItemRunResult = (result['data'].lines[0].result);
            this.populateSTDOUT(result['data'].lines[0].console_output);
            this.stop();
            break;
          case 'METHOD_EXAMPLE_FAILED':
            this.catalogItemStatus = 'RUN_STATUS_FAILED';
            this.populateSTDOUT(result['data'].lines[0].console_output);
            this.populateSTDERR(result['data'].lines[0].error_output);
            this.stop();
            break;
        }
      }, error => {
        throw error;
      });
    }, consoleOutputTimeout);
  }


  /**
   * Stops the interval polling of the catalog item run's console output
   */
  private stopIntervalRequest() {
    clearInterval(this.intervalHandle);
    this.intervalHandle = false;
  }
  

  /**
   * Runs the catalog item
   * 
   * @param event {Object} Event object
   */
  private run(event) {
    this.running = true;

    let inputValuesObject = {};
    for (let i = 0; i < this.catalogItem['inputParameters'].length; i++) {
      inputValuesObject[this.catalogItem['inputParameters'][i].fieldname] = this.inputValues[i];
    }

    let outputValuesObject = {};
    for (let i = 0; i < this.catalogItem['outputParameters'].length; i++) {
      outputValuesObject[this.catalogItem['outputParameters'][i].fieldname] = this.outputValues[i];
    }

    this.api.runCatalogItem(this.id, inputValuesObject, outputValuesObject).then(result => {
      this.catalogItemRunId = result['data'];
      this.startIntervalRequest();
    }).catch(error => {
      throw error;
    });
  }


  /**
   * Stops the execution of the catalog item
   * 
   * @param event {Object} Event object
   */
  private stop(event?) {
    this.running = false;
    this.stopIntervalRequest();
  }


  /**
   * Clears the input form
   * 
   * @param event {Object} Event object
   */
  private clear(event) {
    for (let i = 0; i < this.catalogItem['inputParameters'].length; i++) {
      this.inputValues[i] = '';
    }

    for (let i = 0; i < this.catalogItem['outputParameters'].length; i++) {
      this.outputValues[i] = '';
    }

    this.catalogItemRunSTDOUT = [];
    this.catalogItemRunSTDERR = [];
    this.catalogItemRunResult = '';
    this.catalogItemStatus = 'RUN_STATUS_NULL';
  }
}