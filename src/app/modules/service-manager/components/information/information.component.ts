import { Component, OnInit } from '@angular/core';
import { APIService } from 'app/shared/services/api.service';

@Component({
  templateUrl: './information.component.html',
  styles: []
})
export class InformationComponent implements OnInit {

  public servicesAndScenarios: Array<any> = [];

  constructor(private api: APIService) {}

  ngOnInit() {
    this.api.list(185, 100, 0).then(result => {
      this.servicesAndScenarios = result['aaData'];
    }).catch(error => {
      throw error;
    });
  }
}