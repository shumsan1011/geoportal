import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';

interface ServiceParameter {
  identifier: String;
  title: String;
  description: String;
  widget: String;
};

@Component({
  selector: 'catalog-item-parameter',
  templateUrl: `./catalog-item-parameter.component.html`,
  styles: []
})
export class CatalogItemParameterComponent {
  @Input() type: String;
  @Input() parameter: ServiceParameter;
  @Output() onParameterChange = new EventEmitter<ServiceParameter>();
  @Output() onParameterDelete = new EventEmitter<void>();

  public identifier: String;
  public title: String;
  public description: String;


  /**
   * Triggered on @Input() change
   */
  ngOnChanges() {
    this.identifier = this.parameter.identifier;
    this.title = this.parameter.title;
    this.description = this.parameter.description;
  }


  /**
   * Triggered on model change
   */
  public onModelChange(event) {
    this.onParameterChange.emit({
      identifier: this.identifier,
      title: this.title,
      description: this.description,
      widget: ''
    });
  }


  /**
   * Deletes parameter
   */
  public onDelete(event) {
    this.onParameterDelete.emit();
  }
}
