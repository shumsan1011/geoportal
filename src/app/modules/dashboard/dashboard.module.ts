import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';

import { DashboardRoutingModule } from './dashboard-routing.module';

import { PageMapComponent } from './components/page-map/page-map.component'
import { DashboardPageComponent } from './pages/dashboard/dashboard.page'
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DashboardRoutingModule,
    LeafletModule,
    LeafletDrawModule,
    SharedModule
  ],
  declarations: [
    PageMapComponent,
    DashboardPageComponent
  ],
  exports: [
    DashboardPageComponent
  ],
  entryComponents: [
    DashboardPageComponent,
    PageMapComponent
  ]
})
export class DashboardModule {}