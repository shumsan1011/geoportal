import {
  Component,
  Renderer2,
  ComponentFactoryResolver,
  ViewContainerRef,
  ElementRef,
  ViewChild,
  OnInit
} from '@angular/core';

import { State, Store } from '@ngrx/store';
import { AppState } from 'app/store/app.state';
import { stateGetter } from 'app/store/app.reducer';
import { AppActions } from 'app/store/app.actions';
import { PageMapComponent } from 'app/modules/dashboard/components/page-map/page-map.component';

@Component({
  templateUrl: './dashboard.page.html',
  styles: [
    '.layout__map { width: 100%; height: 40%; position: relative; }',
    '.layout__content { margin: 0.75rem; }',
    '.layout__menu { z-index: 1000; width: 100%; }',
    '.mapcontainer { position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; }'
  ]
})
export class DashboardPageComponent implements OnInit {
  @ViewChild('mapPlacement', { read: ViewContainerRef })
  mapPlacementContainer;
  private user$: Store<any>;

  constructor(
    private store: Store<AppState>,
    private appActions: AppActions,
    private componentFactoryResolver: ComponentFactoryResolver,
    private renderer: Renderer2,
    private el: ElementRef
  ) {}

  ngOnInit() {
    const mapFactory = this.componentFactoryResolver.resolveComponentFactory(PageMapComponent);
    const componentRef = this.mapPlacementContainer.createComponent(mapFactory);

    this.store.select(stateGetter('maps', 'frontPageMap', 'host')).subscribe(data => {
      if (data === 'regular') {
        this.store
          .select(stateGetter('maps', 'frontPageMap', 'ref'))
          .take(1)
          .subscribe(data => {
            this.mapPlacementContainer.insert(data.hostView);
          });
      } else {
        this.mapPlacementContainer.detach(
          this.mapPlacementContainer.indexOf(componentRef.hostView)
        );
        this.appActions.setFrontPageMapRef({ ref: componentRef });
      }
    });

    this.user$ = this.store.select(stateGetter('user'));

    this.appActions.setFrontPageMapHost({ host: 'regular' });
  }

}
