import { Component, ViewChild, Input, OnInit } from '@angular/core';
import { MapComponent } from 'app/shared/components/map/map.component';
import { MapService } from "app/shared/services/map.service";
import { MapToolsAPIService } from "app/shared/api/maptools.api.service";
import {NgForage, NgForageCache, NgForageConfig, CachedItem} from 'ngforage';

import { UUID } from 'angular2-uuid';
import { Store } from '@ngrx/store';
import { stateGetter } from 'app/store/app.reducer';
import { AppActions } from 'app/store/app.actions';

@Component({
  selector: 'gp-page-map',
  templateUrl: './page-map.component.html',
})
export class PageMapComponent extends MapComponent implements OnInit {
  private subscription;
  public layers: Array<any> = [];
  private geometryColumnTypes: Array<String> = ['point', 'line', 'polygon'];

  constructor(public mapTools: MapToolsAPIService, public mapService: MapService,
    public store: Store<any>, public actions: AppActions,
    readonly ngf: NgForage, readonly cache: NgForageCache) {
    super(mapTools, mapService, store, actions, ngf, cache);
    this.mapId = 'maps_front-page-map';
  }
}
